# React快速環境
簡易的React快速環境

## 環境內容 & 支持
- React 15
- Webpack
- Babel
- ES7擴散屬性支持
- LiveReload
- Redux
- Bootstrap 4

## 第一次使用
``` $ npm install ```

## 使用方法一
### Code完畢進行編譯
- ``` $ webpack ```
- 開啟 ``` build/index.html ```

## 使用方法二(LiveReload)
###
- ``` $ npm run dev ```
- 開啟 ``` http://localhost:8080/ ```




