/* main.js */

'use strict'

import React from 'react'
import ReactDOM from 'react-dom'
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'

import { BASENAME } from 'App/env.js'
// import history from './history'
// import createBrowserHistory from 'history/createBrowserHistory';
// const history = createBrowserHistory();

import { createStore,applyMiddleware,combineReducers } from 'redux'
import { Provider } from 'react-redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk';

import 'jquery'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'

import AskTravelInfoReducer from 'AppModules/askTravelInfoModule/reducer'
import TravelCardModeReducer from 'AppModules/travelCardModule/reducer.js'
import MainReducer from 'AppModules/mainModules/reducer.js'
import UserReducer from 'AppModules/userModule/reducer.js'
import ScheduleHistoryReducer from 'AppModules/schedulHistory/reducer.js'
import MyTravelReducer from 'AppModules/myTravelModule/reducer.js'
import AutoModeReducer from 'AppModules/autoScheduleModule/reducer.js'

import Welcome from 'AppPages/welcome.jsx';
import AskTravelInfo from 'AppPages/askTravelInfo.jsx';
import AskAuto from 'AppPages/askAuto.jsx';
import SelectMode from 'AppPages/selectMode.jsx';
import TravelCard from 'AppPages/travelCard.jsx';
import ScheduleResult from 'AppPages/sechduleResult.jsx';
import MemberIndex from 'AppPages/memberIndex.jsx';
import MyTravel from 'AppPages/myTravel.jsx';
import Test from 'AppPages/test.jsx';
import AutoSchedule from 'AppPages/autoSchedule.jsx';
import AutoScheduleOrder from 'AppPages/autoScheduleOrder.jsx';
import RegisterPage from 'AppPages/registerPage.jsx';

import LoadingMask from 'AppModules/mainModules/loadingMask/index.jsx';
import MsgDialog from 'AppModules/mainModules/MsgDialog/index.jsx';
import 'AppSrc/styles/commonStyle.css';

let reducers = combineReducers({
  MainReducer,
  AskTravelInfoReducer,
  TravelCardModeReducer,
  UserReducer,
  ScheduleHistoryReducer,
  MyTravelReducer,
  AutoModeReducer
})

const store = createStore(
  reducers,
  applyMiddleware(thunk),
  applyMiddleware(logger)
);

console.log("redux store",store.getState());

ReactDOM.render(
  <Provider store={store}>
   <div id="root-page">   
    <Router basename={BASENAME}>
        <Switch>
          <Route exact path="/" component={ Welcome } />
          <Route exact path="/index" component={ Welcome } />
          <Route exact path="/ask-travel-info" component={ AskTravelInfo } />
          <Route exact path="/ask-travel-info-auto-mode" component={ AskAuto } />          
          <Route exact path="/select-mode" component={ SelectMode } />
          <Route exact path="/travel-scheduling" component={ TravelCard } />
          <Route exact path="/schedule-result" component={ScheduleResult}/>
          <Route exact path="/member-index" component={MemberIndex}/>
          <Route exact path="/my-travel" component={MyTravel}/>      
          <Route exact path="/auto-scheduling" component={AutoSchedule}/>
          <Route exact path="/order-auto-scheduling" component={AutoScheduleOrder}/>
          <Route exact path="/new-account" component={RegisterPage}/>          
          <Route exact path="/test" component={Test}/>
          <Route component={Welcome} />
        </Switch>       
      </Router >
      <LoadingMask/>
      <MsgDialog/>
   </div>
  </Provider>
  ,
  document.getElementById('root')
);
