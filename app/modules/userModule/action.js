import axios from 'axios';
import { API_HOST } from 'App/env.js';
import { setAppLoadingState, setDialogMsg } from 'AppModules/mainModules/action.js'
export const USER_LOGING_SUCCESS = 'USER_LOGING_SUCCESS';
export const USER_LOGING_FAIL = 'USER_LOGING_FAIL';
export const USER_REGISTER_SUCCESS = 'USER_REGISTER_SUCCESS';
export const USER_REGISTER_FAIL = 'USER_REGISTER_FAIL';

export function register(bundle){
    let {name,email,password} = bundle
    return (dispatch)=>{
        dispatch(setAppLoadingState(true));
        axios.post(API_HOST+'/user-register',{
            name,
            email,
            password
        })
        .then(function(res){
            console.log(res);
            dispatch({type:USER_REGISTER_SUCCESS});
            dispatch(setAppLoadingState(false));
            dispatch(setDialogMsg('帳戶註冊成功',res.data.msg));
        })
        .catch(function(err){
            console.error(err.response)
            dispatch(setAppLoadingState(false));
            dispatch({
                type:USER_REGISTER_FAIL,
                registerErrMsg:err.response.data
            })
        })
    }
}

export function userLogin(email,pwd){
    return (dispatch)=>{
        dispatch(setAppLoadingState(true));
        axios.post(API_HOST+'/user-login',{
            account:email,
            password:pwd
        })
        .then(function(res){
            // console.log(res)
            let {uid,token} = res.data;
            // console.log(uid,token);
            dispatch({
                type:USER_LOGING_SUCCESS,
                uid,
                token
            })
            dispatch(setAppLoadingState(false))
        })
        .catch(function(error){
            console.error(error.response)
            let {msg} = error.response.data
            // dispatch({
            //     type:USER_LOGING_FAIL,
            //     errorMsg:msg
            // })
            dispatch(setDialogMsg('錯誤',msg));
            dispatch(setAppLoadingState(false))
        })
    }
}