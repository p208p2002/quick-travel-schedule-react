import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userLogin } from './action.js';
// import { setDialogMsg } from 'App/modules/mainModules/action.js';
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            remeberMe:false
        }
        this.login = this.login.bind(this);
        this.newAccount = this.newAccount.bind(this);
        this.setCookie = this.setCookie.bind(this);
        this.getCookie = this.getCookie.bind(this);
    }

    componentDidMount() {
        console.log(this.props)
        console.log(this.getCookie('Account'))
        if(this.getCookie('Account') !== ''){
            this.setState({
                remeberMe:true
            })
        }
    }    

    componentWillReceiveProps(nextProps) {
        let { UserReducer } = nextProps;
        let { token = null } = UserReducer;
        let { push } = this.props.history;
        if (token !== null) {
            push('/member-index');
        }
    }

    setCookie(cname,cvalue,exdays)
    {
        var d = new Date();
        d.setTime(d.getTime()+(exdays*24*60*60*1000));
        var expires = "expires="+d.toGMTString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    getCookie(cname)
    {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) 
        {
            var c = ca[i].trim();
            if (parseInt(c.indexOf(name))===0){
                return c.substring(name.length,c.length)
            }
        }
        return "";
    }

    login() {
        // console.log(this.email.value,this.pwd.value);
        let { dispatch } = this.props;
        if(this.state.remeberMe){
            this.setCookie('Account',this.email.value,7);
        }
        else{
            this.setCookie('Account','',0);
        }
        dispatch(userLogin(this.email.value, this.pwd.value));
    }

    newAccount(){
        // let { dispatch } = this.props;        
        this.props.history.push("/new-account");
    }

    render() {
        let { errorMsg } = this.props.UserReducer        
        // console.log();
        let cAccount = this.getCookie('Account');
        return (
            <div className="modal fade bg-gray" id="loginModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <div className="text-center w-100"><h5 className="pa-50">Login QTS</h5></div>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                        <div className="text-center">
                        還沒有帳號嗎?
                                <button
                                    type="button"
                                    className="btn-link"
                                    data-dismiss="modal"
                                    onClick={() => { this.newAccount() }}
                                >
                                    <b>立即註冊</b>
                                </button>
                                <br/>
                        </div>
                            <form className="login-form">
                                {errorMsg ?
                                    <div>
                                        <span className="text-center text-danger w-100 d-block">{errorMsg}</span>
                                        <br />
                                    </div> : null
                                }
                                <div className="form-group">
                                    <input
                                        className="form-control"
                                        type="text"
                                        placeholder="Email"
                                        ref={(input) => { this.email = input; }}
                                        onKeyPress={(e) => {
                                            if (e.key === "Enter") {
                                                this.loginBtn.click()
                                            }
                                        }}
                                        defaultValue={cAccount}
                                    />
                                    <br />
                                    <input
                                        className="form-control"
                                        type="Password"
                                        placeholder="Password"
                                        ref={(input) => { this.pwd = input; }}
                                        onKeyPress={(e) => {
                                            if (e.key === "Enter") {
                                                this.loginBtn.click()
                                            }
                                        }}
                                    />
                                    <div className="d-flex">
                                        <div className="p-2">
                                            <input 
                                                type="checkbox" 
                                                onClick={()=>{
                                                    this.setState({
                                                        remeberMe:!this.state.remeberMe
                                                    })
                                                    if(this.state.remeberMe === true){
                                                        this.setCookie('Account','',0);
                                                    }
                                                }}
                                                checked={this.state.remeberMe}
                                            />
                                            <span>記住帳號</span>
                                        </div>
                                        <div className="ml-auto p-2">
                                            <a href="#">忘記密碼?</a>
                                        </div>
                                    </div>
                                </div>
                                <button
                                    ref={(input)=>{this.loginBtn = input;}}                                    
                                    data-dismiss="modal"
                                    type="button"
                                    onClick={this.login}
                                    className="btn btn-info-c w-100">
                                    登入
                                </button>
                            </form>
                        </div>
                        <div className="text-center">                        
                            <hr />
                            <span className="text-secondary">                                
                                2018 © QTS Team
                            </span>
                            <br />
                            <br />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

LoginForm.propTypes = {
    dispatch: PropTypes.func,
    UserReducer: PropTypes.object,
    history: PropTypes.object
}

let mapStateToPorps = (state) => {
    return {
        UserReducer: state.UserReducer
    }
};

export default withRouter(connect(mapStateToPorps)(LoginForm));