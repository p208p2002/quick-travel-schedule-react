import React, { Component } from 'react';
import { connect } from 'react-redux';
import { register } from './action.js'
import PropTypes from 'prop-types';

class View extends Component {
    constructor(props) {
        super(props);
        this.register = this.register.bind(this)        
    }

    register(){
        let {userName,email,pwd} = this
        userName = userName.value;
        email = email.value;
        pwd = pwd.value;
        // cPwd = cPwd.value;
        let {dispatch} = this.props;        
        let bundle={
            name:userName,
            email,
            password:pwd
        }        
        dispatch(register(bundle));
    }

    render() {
        let {UserReducer} = this.props        
        let { registerErrMsg={} } = UserReducer,
        {msg={}}=registerErrMsg,
        {email:emailErrMsg,name:nameErrMsg,password:passwordErrMsg} = msg;
        emailErrMsg?emailErrMsg=emailErrMsg[0]:undefined;
        nameErrMsg?nameErrMsg=nameErrMsg[0]:undefined;
        passwordErrMsg?passwordErrMsg[0]:undefined;
        return (
            <div className="card bg-alpha text-gray">
                <div className="card-body">
                    <div className="card-title text-center"><h2><b>立即註冊</b></h2></div>
                    <form>
                        <div className="form-group">
                            <label>Name</label>                            
                            <input
                                ref={(input)=>{this.userName=input;}}
                                type="text" 
                                className="form-control"
                                placeholder="請輸入您的名稱..."
                            />
                            {nameErrMsg?<small className="text-danger">{nameErrMsg}</small>:null}
                        </div>

                        <div className="form-group">
                            <label>E-mail</label>
                            <input
                                ref={(input)=>{this.email=input;}}
                                type="text" 
                                className="form-control"
                                placeholder="請輸入您的信箱..."
                            />
                            {emailErrMsg?<small className="text-danger">{emailErrMsg}</small>:null}
                        </div>

                        <div className="form-group">
                            <label>Password</label>
                            <input
                                ref={(input)=>{this.pwd=input;}}
                                type="password" 
                                className="form-control"
                                placeholder="請設定密碼..."
                            />
                            {passwordErrMsg?<small className="text-danger">{passwordErrMsg}</small>:null}
                        </div>                       
                        <div className="text-center">                            
                            <button 
                                type="button"
                                className="btn btn-info-c btn-lg"
                                onClick={this.register}
                            >
                                註冊帳號
                            </button>
                        </div>
                    </form>                    
                </div>
            </div>
        );
    }
}

View.propTypes={
    dispatch:PropTypes.func,
    UserReducer:PropTypes.object
}

let mapStateToProps = (state)=>{
    return{
        UserReducer:state.UserReducer
    }
}
 
export default connect(mapStateToProps)(View);