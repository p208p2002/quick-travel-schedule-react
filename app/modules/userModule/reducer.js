import { 
    USER_LOGING_SUCCESS,
    USER_LOGING_FAIL,
    USER_REGISTER_SUCCESS,
    USER_REGISTER_FAIL
} from './action.js';

const initState=()=>{
    return{
        // uid:undefined,
        token:undefined,
        errorMsg:undefined, //login error msg
        registerErrMsg:undefined
    }
}

export default (state=initState(),action)=>{
    switch (action.type) {
        case USER_LOGING_SUCCESS:
            //set in session       
            sessionStorage.setItem('user',action.token)
            console.log('login',action)
            return Object.assign({},state,{
                // uid:action.uid,
                token:action.token
            })
        
        case USER_LOGING_FAIL:
            return Object.assign({},state,{
                errorMsg:action.errorMsg
            })
        
        case USER_REGISTER_FAIL:
            return Object.assign({},state,{
                registerErrMsg:action.registerErrMsg
            })

        case USER_REGISTER_SUCCESS:
        default:
            return state;
    }
}