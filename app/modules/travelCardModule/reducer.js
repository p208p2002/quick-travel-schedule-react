import {
    TRAVEL_CARD_GET_AREAS,
    TRAVEL_CARD_SELECT_PLACE,
    TRAVEL_CARD_LAST_SELECT_PALCE_ID,
    TRAVEL_CARD_SET_NOW_TIME_FRAGMENT,
    TRAVEL_CARD_SET_NOW_TIME_REMAIN,
    TRAVEL_CARD_SET_NOW_DAY,
    TRAVEL_CARD_REPLACE_REDUCER,
    TRAVEL_CARD_INIT
} from './action'

//reducer
const initstate = ()=>{
    // let fakeInitTulpes = JSON.parse('[{"day":1,"placeId":717,"placeName":"一中商圈","timeFragmentId":1,"timeFragment":1,"trafficAndStayTime":156,"lon":"120.685518","lat":"24.150502"},{"day":1,"placeId":318,"placeName":"逢甲夜市","timeFragmentId":1,"timeFragment":1,"trafficAndStayTime":63,"lon":"120.646327","lat":"24.179879"},{"day":1,"placeId":2120,"placeName":"麗寶樂園","timeFragmentId":2,"timeFragment":2,"trafficAndStayTime":145,"lon":"120.695524","lat":"24.323055"},{"day":1,"placeId":1582,"placeName":"國立臺中自然科博館","timeFragmentId":2,"timeFragment":2,"trafficAndStayTime":155,"lon":"120.665551","lat":"24.156744"},{"day":1,"placeId":621,"placeName":"彩虹村","timeFragmentId":3,"timeFragment":3,"trafficAndStayTime":75,"lon":"120.609971","lat":"24.133942"},{"day":1,"placeId":1222,"placeName":"高美濕地","timeFragmentId":3,"timeFragment":3,"trafficAndStayTime":162,"lon":"120.545184","lat":"24.313276"},{"day":2,"placeId":1040,"placeName":"魚藤坪斷橋","timeFragmentId":1,"timeFragment":1,"trafficAndStayTime":120,"lon":"120.773836","lat":"24.358434"},{"day":2,"placeId":528,"placeName":"台中公園","timeFragmentId":1,"timeFragment":1,"trafficAndStayTime":111,"lon":"120.684461","lat":"24.144834"},{"day":2,"placeId":1137,"placeName":"新社花海","timeFragmentId":2,"timeFragment":2,"trafficAndStayTime":136,"lon":"120.812348","lat":"24.202755"},{"day":2,"placeId":2226,"placeName":"路思義教堂","timeFragmentId":2,"timeFragment":2,"trafficAndStayTime":61,"lon":"120.600554","lat":"24.178895"},{"day":2,"placeId":1937,"placeName":"新社莊園","timeFragmentId":2,"timeFragment":2,"trafficAndStayTime":164,"lon":"120.812436","lat":"24.189487"},{"day":2,"placeId":968,"placeName":"谷關溫泉","timeFragmentId":3,"timeFragment":3,"trafficAndStayTime":154,"lon":"121.009167","lat":"24.203333"},{"day":2,"placeId":847,"placeName":"秋紅谷","timeFragmentId":3,"timeFragment":3,"trafficAndStayTime":114,"lon":"120.639479","lat":"24.168348"},{"day":3,"placeId":2408,"placeName":"Adipiscinonvelitetestconsequaturminus.","timeFragmentId":1,"timeFragment":1,"trafficAndStayTime":133,"lon":"121.1433780","lat":"24.2609910"},{"day":3,"placeId":2572,"placeName":"Ametasperioresestestmagnamsintveritatisincidunt.","timeFragmentId":1,"timeFragment":1,"trafficAndStayTime":111,"lon":"120.9562190","lat":"24.2041890"},{"day":3,"placeId":2159,"placeName":"Sedanimisuntetconsequaturauttotam.","timeFragmentId":2,"timeFragment":2,"trafficAndStayTime":97,"lon":"120.8529850","lat":"24.0308090"},{"day":3,"placeId":2766,"placeName":"Deseruntdoloremquesimiliqueinventoremolestiaemolestiae.","timeFragmentId":2,"timeFragment":2,"trafficAndStayTime":51,"lon":"120.7826850","lat":"24.2832090"},{"day":3,"placeId":1171,"placeName":"Consequaturvoluptatemsitiddolorevelipsamnesciunt.","timeFragmentId":2,"timeFragment":2,"trafficAndStayTime":150,"lon":"120.9025890","lat":"24.2166570"},{"day":3,"placeId":262,"placeName":"Necessitatibusmaioresatuttemporevoluptatequosqui.","timeFragmentId":3,"timeFragment":3,"trafficAndStayTime":67,"lon":"121.0059230","lat":"24.0479860"},{"day":3,"placeId":2956,"placeName":"Utomnisquisvoluptatemsintremesse.","timeFragmentId":3,"timeFragment":3,"trafficAndStayTime":81,"lon":"120.9309310","lat":"24.3633480"}]')
    return{
        cardDatas:[],
        selectPlaceIds:[],
        // selectPlaceTuples:fakeInitTulpes, // 紀錄詳細的選擇地點資料序列 //[]
        selectPlaceTuples:[],
        lastSelectPlaceId:null,
        //用於紀錄目前 card mode 規劃的狀態
        nowDay:1,
        nowTimeFragment:'NO DEFINE',
        nowTimeFragmentId:'0',
        nowTimeFragmentLimitAt:'99:00',
        nowTimeFragmentRemain:null,        
    };
}

export default (state = initstate(),action)=>{
    switch (action.type){
        case TRAVEL_CARD_GET_AREAS:
            return Object.assign({},state,{
                cardDatas:action.cardDatas
            })
        
        case TRAVEL_CARD_SELECT_PLACE:
            // console.log(JSON.stringify(state.selectPlaceTuples));
            //push place in select ary
            let newArray = state.selectPlaceIds.slice();            
            newArray.push(action.placeId)

            //push data in tuple 
            let tuples = state.selectPlaceTuples.slice();
            let newTuple = {
                day:state.nowDay,
                placeId:action.placeId,
                placeName:action.placeName,
                timeFragmentId:state.nowTimeFragmentId,
                timeFragment:action.timeFragment,
                trafficAndStayTime:action.trafficAndStayTime,
                lon:action.lon,
                lat:action.lat
            }
            tuples.push(newTuple)

            //update nowTimeFragmentRemain
            let { nowTimeFragmentRemain } = state
            nowTimeFragmentRemain = nowTimeFragmentRemain - action.trafficAndStayTime            
            return Object.assign({},state,{
                selectPlaceIds:newArray,
                selectPlaceTuples:tuples,
                nowTimeFragmentRemain,                
            })
        
        case TRAVEL_CARD_LAST_SELECT_PALCE_ID:
            return Object.assign({},state,{
                lastSelectPlaceId:action.placeId
            })
        
        case TRAVEL_CARD_SET_NOW_TIME_FRAGMENT:
            return Object.assign({},state,{
                nowTimeFragment:action.nowTimeFragment,
                nowTimeFragmentId:action.nowTimeFragmentId,
                nowTimeFragmentLimitAt:action.nowTimeFragmentLimitAt
            })
        
        case TRAVEL_CARD_SET_NOW_TIME_REMAIN:
            return Object.assign({},state,{
                nowTimeFragmentRemain:action.remainTime
            })
        
        case TRAVEL_CARD_SET_NOW_DAY:
            return Object.assign({},state,{
                nowDay:action.nowDay
            })
        
        case TRAVEL_CARD_REPLACE_REDUCER:            
            return Object.assign({},state,action.reducer);

        case TRAVEL_CARD_INIT:
            return initstate();
        
        default:
            return state
    }
}