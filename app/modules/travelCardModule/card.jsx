import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './card.css'
// import ratingStar from 'AppSrc/icons/rating_start.png'
import placeholder from 'AppSrc/icons/placeholder.png'
import bench from 'AppSrc/icons/bench.png'
import car from 'AppSrc/icons/car.png'
import price from 'AppSrc/icons/get-money.png'
import { selectPlace ,getCards,getRestaurant} from './action'
import { connect } from 'react-redux'
import { TRAVELER_HOST } from 'App/env.js'
// import { TRAVELER_HOST } from '../../env';
class Card extends Component {
    constructor(props){
        super(props)
        this.select = this.select.bind(this)
    }

    select(placeId,trafficAndStayTime,placeName='unknow',lon,lat){        
        if(this.props.timeFragmentId !== 0){
            let { dispatch,stayTime,nowTimeFragmentRemain,trafficTime } = this.props
            // console.log(nowTimeFragmentRemain,stayTime);
            let { 
                selectPlaceIds:withoutPlaceIds,
                nowTimeFragmentId
            } = this.props.travelCardModeReducer            

            dispatch(selectPlace(placeId,nowTimeFragmentId,trafficAndStayTime,placeName,lon,lat))
            console.log(nowTimeFragmentRemain,stayTime,(nowTimeFragmentRemain-stayTime)>30);
            if((nowTimeFragmentRemain-(stayTime+trafficTime))>30){                
                withoutPlaceIds.push(placeId)
                dispatch(getCards(lon,lat,withoutPlaceIds));
            }
            else{
                dispatch(getRestaurant(lon,lat));
                // dispatch(getCards(lon,lat,withoutPlaceIds));
            }
        }
        else{
            console.error('this.props.timeFragmentId invaild',this.props.timeFragmentId)
        }
    }

    render() {
        let {distance,priceStart,priceEnd} = this.props;
        let distanceUnit = '公尺'

        function formatFloat(num, pos)
        {   
            var size = Math.pow(10, pos);
            return Math.round(num * size) / size;
        }

        if(distance>1000){
            distance=formatFloat(distance/1000,1);
            distanceUnit = '公里'
        }

        return (
            <div>
                <div id="travelCard" className="card travel-card d-none d-md-flex" style={{height:'650px'}}>
                    <div className="card-header">
                    <small><img src={placeholder} width="16" alt="位於:"/>{this.props.address}</small>
                    </div>
                    <img className="card-img" src={this.props.bgImgurl} alt="Card image"/>
                    <div className="card-body">
                        <h5 className="card-title"><b>{this.props.locationName}</b></h5>
                        <div className="card-text">
                            <span><img src={bench} width="24px" alt="預估的停留時間:"/><b>停留&nbsp;</b>{this.props.stayTime}分鐘</span><br/>
                            <span><img src={car} width="24px" alt="預估的交通時間:"/><b>交通&nbsp;</b>{this.props.trafficTime}分鐘,&nbsp;{distance}{distanceUnit}</span><br/> 
                            {/* <span><img src={ratingStar} width="23px" alt="評價:"/><b>評價&nbsp;</b>{this.props.placeRating}</span><br/> */}
                            <span><img src={price} width="24px" alt="評價:"/><b>花費&nbsp;</b>{priceStart}&nbsp;-&nbsp;{priceEnd}元</span><br/>
                            <br/>                       
                            <p>{this.props.content}</p>                      
                        </div>                    
                    </div>
                    <div className="card-footer">
                        <small>
                            查看
                            <b>{this.props.locationName}</b>
                            在
                            <b>Traveler</b>
                            上的
                            <a href={ TRAVELER_HOST+'/place/show/'+this.props.placeId } target="_blank">景點介紹</a>
                            與<a href="#">旅遊文章</a>
                        </small>                    
                    </div>
                    <button 
                            type="button" 
                            className="btn btn-info-c btn-lg btn-align"
                            onClick={()=>{
                                let {placeId,stayTime,trafficTime,locationName,lon,lat} = this.props
                                this.select(placeId,(stayTime+trafficTime),locationName,lon,lat)
                            }}
                        >                            
                            選擇
                    </button>
                </div>
                
                {/* mobile */}
                {/* style={{maxHeight:'250px'}} */}
                <div id="travelCard-m" className="card travel-card d-md-none" >
                    <small className="w-100 text-center">
                            探索
                            <b>{this.props.locationName}</b>
                            的
                            <a href={ TRAVELER_HOST+'/place/show/'+this.props.placeId } target="_blank">景點介紹</a>
                            與<a href="#">旅遊文章</a>
                    </small>        
                    <img className="card-img" src={this.props.bgImgurl} alt="Card image"/>
                    <div className="img-mask"/>
                    <div className="text-area">
                        <div className="w-100 text-center"><b>{this.props.locationName}</b></div>
                        <br/>
                        <div className="card-text w-100 text-center">
                            <span><img src={bench} width="24px" alt="預估的停留時間:"/><b>停留&nbsp;</b>{this.props.stayTime}分鐘</span><br/>
                            <span><img src={car} width="24px" alt="預估的交通時間:"/><b>交通&nbsp;</b>{this.props.trafficTime}分鐘,&nbsp;{distance}{distanceUnit}</span><br/> 
                            {/* <span><img src={ratingStar} width="23px" alt="評價:"/><b>評價&nbsp;</b>{this.props.placeRating}</span><br/> */}
                            <span><img src={price} width="24px" alt="評價:"/><b>花費&nbsp;</b>{priceStart}&nbsp;-&nbsp;{priceEnd}元</span><br/>
                            <br/>                                                   
                        </div>                    
                    </div>
                    <button 
                            type="button" 
                            className="btn btn-info-c btn-lg btn-align"
                            onClick={()=>{
                                let {placeId,stayTime,trafficTime,locationName,lon,lat} = this.props
                                this.select(placeId,(stayTime+trafficTime),locationName,lon,lat)
                            }}
                        >                            
                            選擇
                    </button>
                </div>
            </div>
        )
    }
}

Card.propTypes={
    locationName:PropTypes.string,
    address:PropTypes.string,
    content:PropTypes.string,
    stayTime:PropTypes.number,
    dispatch:PropTypes.func,
    placeId:PropTypes.number,
    // placeRating:PropTypes.number,
    travelCardModeReducer:PropTypes.object,
    bgImgurl:PropTypes.string,
    trafficTime:PropTypes.number,
    timeFragmentId:PropTypes.number,
    lon:PropTypes.number,
    lat:PropTypes.number,
    distance:PropTypes.number,
    priceStart:PropTypes.number,
    priceEnd:PropTypes.number,
    nowTimeFragmentRemain:PropTypes.number
}

let mapToProps = (state)=>{
    return{
        travelCardModeReducer:state.TravelCardModeReducer
    }
}

export default connect(mapToProps)(Card);