import axios from 'axios';
import { API_HOST } from 'App/env.js'
import { setAppLoadingState,setDialogMsg } from 'AppModules/mainModules/action.js'

export const TRAVEL_CARD_GET_AREAS =  'TRAVEL_CARD_GET_AREAS'
export const TRAVEL_CARD_SELECT_PLACE = 'TRAVEL_CARD_PLACE_SELECT'
export const TRAVEL_CARD_LAST_SELECT_PALCE_ID = 'TRAVEL_CARD_LAST_SELECT_PALCE_ID'

export const TRAVEL_CARD_SET_NOW_TIME_FRAGMENT  = 'TRAVEL_CARD_SET_NOW_TIME_FRAGMENT'
export const TRAVEL_CARD_SET_NOW_TIME_REMAIN = 'TRAVEL_CARD_SET_NOW_TIME_REMAIN'
export const TRAVEL_CARD_SET_NOW_DAY = 'TRAVEL_CARD_SET_NOW_DAY'

export const TRAVEL_CARD_SAVE_SCHEDULE_SUCCESS = 'TRAVEL_CARD_SAVE_SCHEDULE';
export const TRAVEL_CARD_SAVE_SCHEDULE_FAIL = 'TRAVEL_CARD_SAVE_SCHEDULE';

export const TRAVEL_CARD_REPLACE_REDUCER = 'TRAVEL_CARD_REPLACE_REDUCER';

export const TRAVEL_CARD_GET_RESTAURANT_SUCCESS = 'TRAVEL_CARD_GET_RESTAURANT_SUCCESS';
export const TRAVEL_CARD_GET_RESTAURANT_FAIL = 'TRAVEL_CARD_GET_RESTAURANT_FAIL';

export const TRAVEL_CARD_INIT = 'TRAVEL_CARD_INIT';


//action
export function setNowDay(nowDay){
    return{
        type:TRAVEL_CARD_SET_NOW_DAY,
        nowDay
    }
}

export function setNowTimeRemain(remainTime){
    return{
        type:TRAVEL_CARD_SET_NOW_TIME_REMAIN,
        remainTime
    }
}

//timeFragment : 早上 下午 晚上
//接受一個標記 然後轉換成 id 並 取得該區間截止時間
export function setNowTimeFragment(timeFragment,resetNowTimeRemain=false,orderTimeReamin=null){  
    //取得該區間截止時間
    let endTime = (fragmentTime)=>{
        switch (fragmentTime) {
            case '早上':
                return '12:00'

            case '下午':
                return '18:00'

            case '晚上':
                return '22:00'
    
            default:
                return 'invaild'                    
        }
    }
    let timeFragmentLimitAt = endTime(timeFragment)

    //然後轉換成 id 
    let timeFragmentId = 0       
    switch (timeFragment) {        
        case '早上':
        timeFragmentId = 1
            break;

        case '下午':
        timeFragmentId = 2
            break;

        case '晚上':
        timeFragmentId = 3
            break;
    
        default:
            break;
    }    

    return (dispatch)=>{
        dispatch({
            type:TRAVEL_CARD_SET_NOW_TIME_FRAGMENT,
            nowTimeFragment:timeFragment,
            nowTimeFragmentId:timeFragmentId,        
            nowTimeFragmentLimitAt:timeFragmentLimitAt
        })

        if(resetNowTimeRemain){         
            if(orderTimeReamin !== null){
                dispatch(setNowTimeRemain(orderTimeReamin))
            }
            else{
                dispatch(setNowTimeRemain(240))
            }
        }
    }
}

export function getRestaurant(lon,lat){    
    return (dispatch)=>{
        dispatch({type:'MAIN_MODULE_SET_APP_LOADING',loadingState:true})        
        axios.post(API_HOST+'/recommend-food',{
            token:sessionStorage.getItem('user'),
            lon,
            lat,
            range:3500,            
        })
            .then(function(res){                
                console.log('getFood',res)
                dispatch({
                    type:TRAVEL_CARD_GET_AREAS,
                    cardDatas:res.data
                })                
                dispatch({type:'MAIN_MODULE_SET_APP_LOADING',loadingState:false})
            })
            .catch(function(error){                
                dispatch({type:'MAIN_MODULE_SET_APP_LOADING',loadingState:false})                
                dispatch(setDialogMsg('錯誤',error.response.data.msg))
                dispatch(setAppLoadingState(false)) 
            })
    }
}


export function getCards(selfLon,selfLat,withOutPlaceIds=[]){
    return (dispatch)=>{
        dispatch({type:'MAIN_MODULE_SET_APP_LOADING',loadingState:true})
        axios.post(API_HOST+'/get-nearby-place',{
            token:sessionStorage.getItem('user'),
            lon:selfLon,
            lat:selfLat,
            range:5000,
            with_out_place_ids:withOutPlaceIds                        
        })
            .then(function(res){
                console.log('getCard',res)
                dispatch({
                    type:TRAVEL_CARD_GET_AREAS,
                    cardDatas:res.data
                })                
                dispatch({type:'MAIN_MODULE_SET_APP_LOADING',loadingState:false})
            })
            .catch(function(error){
                dispatch({type:'MAIN_MODULE_SET_APP_LOADING',loadingState:false})                
                dispatch(setDialogMsg('錯誤',error.response.data.msg))
                dispatch(setAppLoadingState(false)) 
        })
    }        
}

//timeFragment 1:早上 2:中午 3:晚上
export function selectPlace(placeId,timeFragment,trafficAndStayTime,placeName,lon,lat){
    console.log('select place',placeId,timeFragment,trafficAndStayTime,lon,lat)
    return{
        type:TRAVEL_CARD_SELECT_PLACE,
        placeId,
        timeFragment,
        trafficAndStayTime,
        placeName,
        lon,
        lat
    }
}

export function saveSchedule(reducers){    
    return (dispatch)=>{
        dispatch(setAppLoadingState(true));
        let user = sessionStorage.getItem('user')

        return axios.post(API_HOST+'/save-travel-schedule',{
            token:user,
            reducers:JSON.stringify(reducers)
        }
        )
        .then(function(res){
            console.log(res.data)
            dispatch({
                type:TRAVEL_CARD_SAVE_SCHEDULE_SUCCESS,                
            })
            dispatch(setAppLoadingState(false));
            return Promise.resolve(res.data.travelRecordId);
        })
        .catch(function(err){
            console.error(err)
            dispatch(setDialogMsg('錯誤','旅遊建檔失敗'));
            dispatch(setAppLoadingState(false));
        })

    }
}

export function replaceReducer(reducer){    
    return{
        type:TRAVEL_CARD_REPLACE_REDUCER,
        reducer
    }
}

export function initTravelCard(){
    return{
        type:TRAVEL_CARD_INIT
    }
}