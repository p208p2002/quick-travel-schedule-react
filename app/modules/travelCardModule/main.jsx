import React, { Component } from 'react';
import './style.css'
import { connect } from 'react-redux'
import { getCards,setNowTimeRemain,setNowTimeFragment,setNowDay,saveSchedule } from './action'
import PropTypes from 'prop-types';
import Card from './card.jsx'
import {singleTimeJudge,timeInterval,getNowTimeStartAt} from 'AppCore/timeTools.js'
import { withRouter } from "react-router-dom";
import { IMG_SERVER_HOST } from 'App/env.js';
import { setDialogMsg } from 'AppModules/mainModules/action.js'

class Main extends Component {
    constructor(props){
        super(props)
        this.state={
            nowTimeFragment:null, //早上 中午 晚上
            timeFragmentRemain:null,
            isFinalTimeFragment:false
        }
        this.showSelcetPlace = this.showSelcetPlace.bind(this)
        // this.calTimeFragmentRemain = this.calTimeFragmentRemain.bind(this)        
    }

    componentWillMount(){        
        const {
            askTravelInfoReducer={},
            travelCardModeReducer={},
            dispatch
        } = this.props
        const { inputBundle={} } = askTravelInfoReducer
        const {startAtTime=''} = inputBundle

        let nowTimeFragment = singleTimeJudge(startAtTime) //目前是 早 中 晚?       
        let nowTimeDeadline = travelCardModeReducer.nowTimeFragmentLimitAt //區段截止時間       

        //計算區間剩餘時間
        let timeFragmentRemain = timeInterval(startAtTime,nowTimeDeadline)
        console.log('endTimeFragment',timeFragmentRemain)

        //save
        this.setState({
            nowTimeFragment,
            timeFragmentRemain
        })
        dispatch(setNowTimeRemain(timeFragmentRemain))
    }

    componentDidMount(){             
        let {dispatch,askTravelInfoReducer} = this.props,
        { inputBundle } = askTravelInfoReducer,
        { startAtLatLon } = inputBundle;

        
        console.log('mount',startAtLatLon);
        dispatch(getCards(startAtLatLon.lon,startAtLatLon.lat));
        // console.log('state',this.state)
    }

    componentWillReceiveProps(nextProps){
        let { isFinalTimeFragment } = this.state; 
        let { askTravelInfoReducer } = this.props;
        let { days:totalTravelDays } = askTravelInfoReducer.inputBundle
        let { travelCardModeReducer} = nextProps;
        let { nowTimeFragmentRemain,nowTimeFragment } = travelCardModeReducer;
        let { nowDay } = this.props.travelCardModeReducer
        let { dispatch } = this.props;                
        let  {endAtTime} = askTravelInfoReducer.inputBundle;
        let endAtTimeFragment = singleTimeJudge(endAtTime);
        let { history } = this.props
		
		//判定是否要結束規劃
		if(isFinalTimeFragment && nowTimeFragmentRemain<=30){            
            let reducers = {
                askTravelInfoReducer:nextProps.askTravelInfoReducer,
                travelCardModeReducer:nextProps.travelCardModeReducer
            }

            dispatch(saveSchedule(reducers))
            .then(function(res){
                console.log(res);
                history.push('/schedule-result?sid='+res);
            });            
            return
		}

        //判定是否最後一天最後一區段
		if( isFinalTimeFragment===false
			&& parseInt(nowDay) === parseInt(totalTravelDays) 
			&& nowTimeFragment === endAtTimeFragment
		){            
            this.setState({isFinalTimeFragment:true})
            let nowTimeStartAt = getNowTimeStartAt(endAtTimeFragment);
            let finalTimeFragmentRemain = timeInterval(nowTimeStartAt,endAtTime);                        
            dispatch(setNowTimeFragment(endAtTimeFragment,true,finalTimeFragmentRemain)); //設定最後區段            
		}
		//否則判斷切換區間  
        else if( (nowTimeFragmentRemain<=30) && (isFinalTimeFragment===false)){
            console.log('switch nowTimeFragment',nowTimeFragmentRemain)
			switch (nowTimeFragment) {
				case '早上':
					dispatch(setNowTimeFragment('下午',true));       
					break;
				
				case '下午':
					dispatch(setNowTimeFragment('晚上',true));       
					break;
				
				case '晚上':
					dispatch(setNowTimeFragment('早上',true));
					dispatch(setDialogMsg('提醒','現在開始規劃第'+(nowDay+1)+'天'));
					dispatch(setNowDay(nowDay+1));
					break;
				default:
					console.error('no time fragment case');
					break;
			}            
        }        
    }

    showSelcetPlace(timeFragmentId){
        let {selectPlaceTuples=[],nowDay} = this.props.travelCardModeReducer
        let filterResult = selectPlaceTuples.filter((tuple)=>{
            if(parseInt(tuple.timeFragmentId) === parseInt(timeFragmentId)){
                return true;
            }
            return false;
        })
        let output = ''

        //篩選 只顯示目前規劃
        filterResult = 
            filterResult.filter((data)=>{
                if(data.day === nowDay){
                    return true;
                }
                return false;
            });

        //展開
        filterResult.map((data)=>{
            output = output+data.placeName + ','
        })
        return(
            <span>{output}</span>
        )
        
    }
    

    render() {
        const {cardDatas:cardDatasAry=[]} = this.props.travelCardModeReducer        
        const {askTravelInfoReducer={},travelCardModeReducer={}} = this.props
        const { inputBundle={} } = askTravelInfoReducer
        const {days:totalTravelDays=''} = inputBundle        
        // const {startAtTime='',endAtTime=''} = inputBundle

        
        let {nowTimeFragment} = travelCardModeReducer //早 中 晚?
        // let endTime = travelCardModeReducer.nowTimeFragmentLimitAt //目前區段截止時間

        //計算該區間剩餘時間
        let {nowTimeFragmentRemain} = travelCardModeReducer        

        //
        // let {selectPlaceIds=[]} = travelCardModeReducer        
        let {nowTimeFragmentId} = travelCardModeReducer
        let {nowDay} = travelCardModeReducer
        

        return ( 
            <div id="travel-cards h-100">
                {/*  */}
                {/* <p>開發工具</p>
                <button type="button" onClick={()=>{
                    this.props.dispatch(setNowTimeFragment('早上'))
                }}>時間區段:早上</button>                
                <button type="button" onClick={()=>{
                    this.props.dispatch(setNowTimeFragment('下午'))
                }}>時間區段:下午</button>
                <button type="button" onClick={()=>{
                    this.props.dispatch(setNowTimeFragment('晚上'))
                }}>時間區段:晚上</button>
                <button
                    type="button"
                    onClick={()=>{                        
                        this.props.history.push('/schedule-result')
                    }}
                >
                結束規劃
                </button>
                <button 
                    type="button"
                    onClick={()=>{                                                
                        let newTravelCardModeReducer = Object.assign({},travelCardModeReducer,{
                            cardDatas:{}
                        });                        
                        let reducers = {
                            askTravelInfoReducer,
                            travelCardModeReducer:newTravelCardModeReducer
                        }
                        let {dispatch} = this.props;
                        
                        dispatch(saveSchedule(reducers))
                        .then(function(res){
                            console.log(res);
                        });
                    }}
                >
                    log and save
                </button>
                <br/>
                <span className="text-danger">區間剩餘時間:{nowTimeFragmentRemain}</span>
                <hr/> */}
                {/*  */}
                <br/>     
                <div className="row"> 
                    <div className="col-12">                        
                        <h3>現在規劃:第{nowDay}天{nowTimeFragment}，共{totalTravelDays}天</h3>
                        <span><b>早上:</b>{this.showSelcetPlace(1)}</span><br/>
                        <span><b>中午:</b>{this.showSelcetPlace(2)}</span><br/>
                        <span><b>晚上:</b>{this.showSelcetPlace(3)}</span><br/>
                        <br/>
                    </div>

                    <div className="col-12">
                        <div className="row">
                        {cardDatasAry.map((data,index)=>                                                        
                                <div key = {index} className="col-12 col-md-4">
                                    <Card
                                        placeId={data.id}
                                        placeRating={'尚未有資訊'}
                                        // bgImgurl={IMG_SERVER_HOST+data.place_img_url}
                                        bgImgurl={/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/.test(data.place_img_url)?data.place_img_url:IMG_SERVER_HOST+data.place_img_url}
                                        locationName={data.title}
                                        address={data.address}                                        
                                        stayTime={data.stay_time}
                                        lon={data.longitude}
                                        lat={data.latitude}                                        
                                        timeFragmentId={nowTimeFragmentId}
                                        content = {data.recommendation}
                                        trafficTime={Math.round(data.trafic_info_relative_last_select.duration.value/60)}
                                        distance={data.trafic_info_relative_last_select.duration.value}
                                        priceStart={data.price_start}
                                        priceEnd={data.price_end}
                                        nowTimeFragmentRemain={nowTimeFragmentRemain}
                                    />
                                    <br/>   
                                </div>
                        )}
                        </div> 
                    </div>                    
                </div>
            </div>
         )
    }
}

Main.propTypes={
    dispatch : PropTypes.func,
    travelCardModeReducer : PropTypes.object,
    askTravelInfoReducer: PropTypes.object,
    history:PropTypes.object
}

let mapStateToProps = (state)=>{
    return{
        travelCardModeReducer:state.TravelCardModeReducer,
        askTravelInfoReducer:state.AskTravelInfoReducer
    }
}
 
export default withRouter(connect(mapStateToProps)(Main));