import { 
    AUTO_SCHEDULE_SAVE_POINT,
    AUTO_SCHEDULE_START_SCHEDULE,
    AUTO_SCHEDULE_SET_NOW_PLAN,
    AUTO_SCHEDULE_SET_RESTARUANT_FALG,
    AUTO_SCHEDULE_INIT,
    AUTO_SCHEDULE_ADD_ALREADY_SCHEDULE,
    AUTO_SCHEDULE_FINISH,
    AUTO_SCHEDULE_ORDERLIST_REMOVE_FIRST_OBJ,
    AUTO_SCHEDULE_GET_A2B_PLACE_INFO,
    AUTO_SCHEDULE_SET_WITH_OUT_IDS
} from './action.js'

const initstate = ()=>{
    return{
        selectPlaceIds:[],
        selectPlaceTuples:[],
        nowPlanDay:0,
        nowPlanTimeFragmentId:0,
        nowPlanTimeFragmentRemain:0,
        nowPlanLon:0,
        nowPlanLat:0,
        needResaurantFlag:false,
        alreadyShceduleTimeFragment:0,
        scheduleIsFinish:false,
        orderList:[]
    };
}

export default (state=initstate(),action)=>{
    let { alreadyShceduleTimeFragment } = state
    switch (action.type) {
        case AUTO_SCHEDULE_SAVE_POINT:
            var {selectPlaceTuples,selectPlaceIds,nowPlanTimeFragmentRemain} = state
            selectPlaceTuples.push(action.selectPoint)
            selectPlaceIds.push(action.selectPoint.placeId)
            nowPlanTimeFragmentRemain = nowPlanTimeFragmentRemain - parseInt(action.selectPoint.trafficAndStayTime)
            console.log('現在區段剩餘時間:',nowPlanTimeFragmentRemain);
            return Object.assign({},state,{
                selectPlaceTuples,
                selectPlaceIds,
                nowPlanTimeFragmentRemain,
                nowPlanLat:action.nowPlanLat,
                nowPlanLon:action.nowPlanLon,
                needResaurantFlag:false,            
            })
        
        case AUTO_SCHEDULE_SET_WITH_OUT_IDS:
            return Object.assign({},state,{
                selectPlaceIds:action.withOutIds
            })

        case AUTO_SCHEDULE_SET_NOW_PLAN:
            var {nowPlanDay,nowPlanTimeFragmentId,nowPlanTimeFragmentRemain} = state            
            return Object.assign({},state,{
                nowPlanDay:action.day?action.day:nowPlanDay,
                nowPlanTimeFragmentId:action.timeFragmentId?action.timeFragmentId:nowPlanTimeFragmentId,
                nowPlanTimeFragmentRemain:action.timeFragmentRemain?action.timeFragmentRemain:nowPlanTimeFragmentRemain,                
            })
       
        case AUTO_SCHEDULE_SET_RESTARUANT_FALG:            
            alreadyShceduleTimeFragment++
            return Object.assign({},state,{
                needResaurantFlag:action.needResaurantFlag,
                alreadyShceduleTimeFragment
            })

        case AUTO_SCHEDULE_ADD_ALREADY_SCHEDULE:
            alreadyShceduleTimeFragment = alreadyShceduleTimeFragment + action.addValue
            return Object.assign({},state,{
                alreadyShceduleTimeFragment
            })
        
        case AUTO_SCHEDULE_ORDERLIST_REMOVE_FIRST_OBJ:
            return Object.assign({},state,{
                orderList:action.orderList
            })

        case AUTO_SCHEDULE_FINISH:
            return Object.assign({},state,{
                scheduleIsFinish:true
            })

        case AUTO_SCHEDULE_INIT:
            return initstate();

        case AUTO_SCHEDULE_GET_A2B_PLACE_INFO:
        case AUTO_SCHEDULE_START_SCHEDULE:
        default:
            return state;
    }
}