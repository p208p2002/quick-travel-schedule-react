import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {startSchedule,setNowPlan,
    setRestaurantFlag,addAlreadySchedule,
    scheduleFinish
} from './action.js';
import {saveSchedule} from 'AppModules/travelCardModule/action.js';
import {getTimeFragmentLimitAt,getTimeFragmentStartAt,timeInterval} from 'AppCore/timeTools.js'
import { withRouter } from "react-router-dom";
import './index.css'

class AutoSchedule extends Component {
    constructor(props) {
        super(props);
        // this.state = {  }
        this.countHowManyTimeFragment = this.countHowManyTimeFragment.bind(this)
    }

    componentDidMount(){        
        let { dispatch,AskTravelInfoReducer } = this.props,
        { inputBundle={} } = AskTravelInfoReducer,
        {startAtLatLon,startAtTimeId=1,startAtTime}=inputBundle;        

        //fake
        // inputBundle = JSON.parse('{"area":{"id":1,"name":"台中","longitude":"120.6736482","latitude":"24.1477358"},"days":"2","isSelectHotel":"false","startAt":"台灣台中東區火車站(東站)","endAt":"台灣台中市烏日站區二路高鐵台中站","orderHotelList":[],"startAtTime":"08:00","endAtTime":"21:30","startAtLatLon":{"lat":24.1373261,"lon":120.68678879999993},"endAtLatLon":{"lat":23.8820205,"lon":120.94549559999996},"startAtTimeId":1,"endAtTimeId":3}')
        // startAtLatLon = inputBundle.startAtLatLon
        // startAtTime = inputBundle.startAtTime


        // console.log(
        //     inputBundle,
        //     getTimeFragmentLimitAt(startAtTimeId),
        //     timeInterval(startAtTime,getTimeFragmentLimitAt(startAtTimeId))
        // );

        let timeRemain = timeInterval(startAtTime,getTimeFragmentLimitAt(startAtTimeId))
        dispatch(setNowPlan(1,startAtTimeId,timeRemain))
        dispatch(startSchedule(startAtLatLon.lon,startAtLatLon.lat,1,startAtTimeId));        
    }

    componentWillReceiveProps(nextProps){
        console.log('next',nextProps);
        let  {nowPlanLon,nowPlanLat,selectPlaceIds,
            nowPlanTimeFragmentId,nowPlanDay,nowPlanTimeFragmentRemain,
            needResaurantFlag,scheduleIsFinish
        } = nextProps.AutoModeReducer

        if(scheduleIsFinish){
            return
        }

        let { inputBundle={} } = this.props.AskTravelInfoReducer
        let { history } = this.props
        
        //fake
        // inputBundle = JSON.parse('{"area":{"id":1,"name":"台中","longitude":"120.6736482","latitude":"24.1477358"},"days":"2","isSelectHotel":"false","startAt":"台灣台中東區火車站(東站)","endAt":"台灣台中市烏日站區二路高鐵台中站","orderHotelList":[],"startAtTime":"08:00","endAtTime":"21:30","startAtLatLon":{"lat":24.1373261,"lon":120.68678879999993},"endAtLatLon":{"lat":23.8820205,"lon":120.94549559999996},"startAtTimeId":1,"endAtTimeId":3}')
        
        let { endAtTimeId,days,endAtTime } =inputBundle;
        endAtTimeId = parseInt(endAtTimeId)
        days = parseInt(days)
        let {dispatch} = this.props

        let judgeLastTimefragment = (switchTimeFragmentId=nowPlanTimeFragmentId)=>{
            if(nowPlanDay === days && switchTimeFragmentId === endAtTimeId){
                return true
            }
            return false;
        }

        //如果剩餘時間>=30分鐘繼續規劃
        if(nowPlanTimeFragmentRemain>=30){            
            dispatch(startSchedule(nowPlanLon,nowPlanLat,nowPlanDay,nowPlanTimeFragmentId,selectPlaceIds,needResaurantFlag===true?2:1))
        }
        else{                        

            // 切換時區或是換天
            let fragmentStart;
            if(nowPlanDay !== days || nowPlanTimeFragmentId !== endAtTimeId){ //如果還沒有規劃完成
                if(nowPlanTimeFragmentId === 1){
                    dispatch(setRestaurantFlag(true))
                    if(!judgeLastTimefragment(2)){
                        dispatch(setNowPlan(undefined,2,240))
                    }
                    else{ //如果下一區間是最後                        
                        fragmentStart =  getTimeFragmentStartAt(2);
                        dispatch(setNowPlan(undefined,2,timeInterval(fragmentStart,endAtTime)))
                    }                    
                }
                else if(nowPlanTimeFragmentId === 2){
                    dispatch(setRestaurantFlag(true))
                    if(!judgeLastTimefragment(3)){
                        dispatch(setNowPlan(undefined,3,240))
                    }
                    else{ //如果下一區間是最後
                        fragmentStart =  getTimeFragmentStartAt(3);
                        dispatch(setNowPlan(undefined,3,timeInterval(fragmentStart,endAtTime)))
                    }                    
                }
                else if(nowPlanTimeFragmentId === 3){
                    dispatch(setRestaurantFlag(true))
                    if(!judgeLastTimefragment(1)){
                        dispatch(setNowPlan(nowPlanDay+1,1,240))
                    }
                    else{ //如果下一區間是最後
                        fragmentStart =  getTimeFragmentStartAt(1);
                        dispatch(setNowPlan(nowPlanDay+1,1,timeInterval(fragmentStart,endAtTime)))
                    }                                        
                }
                else{
                    console.error('切換時區或天發生錯誤');
                }
            }
            else{//規劃完了
                console.log('規劃完成')
                dispatch(addAlreadySchedule())
                dispatch(scheduleFinish())
                dispatch(saveSchedule({
                    travelCardModeReducer:nextProps.AutoModeReducer,
                    askTravelInfoReducer:this.props.AskTravelInfoReducer
                }))
                .then(function(res){
                    history.push('/schedule-result?sid='+res);
                });
                return
            }
        }
    }
 
    //計算總共多少時間區間
    countHowManyTimeFragment(){
        let { AskTravelInfoReducer } = this.props,
        { inputBundle={} } = AskTravelInfoReducer,
        { days,endAtTimeId=0,startAtTimeId=0 } = inputBundle
        startAtTimeId = parseInt(startAtTimeId)
        endAtTimeId = parseInt(endAtTimeId)
        days = parseInt(days)
        let totalTimeFragment = 0;
        for(let i=1;i<=days;i++){
            let offset = 0
            if(i===1){
                offset = offset + (startAtTimeId-1)*-1
            }
            if(i===days){
                offset = offset +
                 endAtTimeId-3                
            }

            console.log(i,days)
            totalTimeFragment = totalTimeFragment + 3 + offset
        }

        return totalTimeFragment
    }

    render() {
        let { AskTravelInfoReducer,AutoModeReducer } = this.props,
        { inputBundle } = AskTravelInfoReducer,
        { nowPlanDay,nowPlanTimeFragmentId,alreadyShceduleTimeFragment } =AutoModeReducer,
        { days } = inputBundle;
        //nowPlanTimeFragmentRemain
        console.log(AskTravelInfoReducer,AutoModeReducer);
        let totalTimeFragment = this.countHowManyTimeFragment();

        let progressStatus = Math.round(alreadyShceduleTimeFragment/totalTimeFragment*100);
        progressStatus>100?'100':String(progressStatus)
        
        return (
            <div id="autoSchedule">
            <br/>
                <div className="progerss-info">
                    <p>進度:{progressStatus}%</p>
                    <p>現在規劃(天):{nowPlanDay}/{days}</p>
                    <p>現在規劃時段:{nowPlanTimeFragmentId}</p>
                    <p>已經完成規劃時段:{alreadyShceduleTimeFragment}/{totalTimeFragment}</p>                    
                </div>

                {/* <div className="progerss-info">
                    <p>進度:{alreadyShceduleTimeFragment/totalTimeFragment}</p>
                    <p>旅遊天數:{days}</p>
                    <p>總時段數:{totalTimeFragment}</p>
                    <hr/>
                    <p>現在規劃:{nowPlanDay}/{days}</p>
                    <p>現在規劃時段:{nowPlanTimeFragmentId}</p>
                    <p>現在時段剩餘:{nowPlanTimeFragmentRemain}</p>
                    <p>已經規劃時段數:{alreadyShceduleTimeFragment}</p>
                </div> */}

               <div className="progress">
                    <div 
                        key={progressStatus}
                        className="progress-bar progress-bar-striped progress-bar-animated" 
                        role="progressbar" 
                        aria-valuemin="0" 
                        aria-valuemax="100" 
                        aria-valuenow={progressStatus}
                        style={{'width': progressStatus + '%'}}
                    />
                </div>
            </div>
        );
    }
}

let mapStatetoProps=(state)=>{
    return{
        AskTravelInfoReducer:state.AskTravelInfoReducer,
        AutoModeReducer:state.AutoModeReducer
    }
}

AutoSchedule.propTypes={
    AskTravelInfoReducer:PropTypes.object,
    AutoModeReducer:PropTypes.object,
    dispatch:PropTypes.func,
    history:PropTypes.func
}

export default withRouter(connect(mapStatetoProps)(AutoSchedule));
