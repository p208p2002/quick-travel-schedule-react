import axios from 'axios';
import { API_HOST } from 'App/env.js'

export const AUTO_SCHEDULE_START_SCHEDULE = 'AUTO_SCHEDULE_START_SCHEDULE';
export const AUTO_SCHEDULE_SAVE_POINT = 'AUTO_SCHEDULE_SAVE_POINT';
export const AUTO_SCHEDULE_SET_NOW_PLAN = 'AUTO_SCHEDULE_SET_NOW_PLAN';
export const AUTO_SCHEDULE_SET_RESTARUANT_FALG = 'AUTO_SCHEDULE_SET_RESTARUANT_FALG';
export const AUTO_SCHEDULE_INIT = 'AUTO_SCHEDULE_INIT';
export const AUTO_SCHEDULE_ADD_ALREADY_SCHEDULE = 'AUTO_SCHEDULE_ADD_ALREADY_SCHEDULE';
export const AUTO_SCHEDULE_FINISH = 'AUTO_SCHEDULE_FINISH';
export const AUTO_SCHEDULE_GET_A2B_PLACE_INFO = 'AUTO_SCHEDULE_GET_A2B_PLACE_INFO';
export const AUTO_SCHEDULE_ORDERLIST_REMOVE_FIRST_OBJ = 'AUTO_SCHEDULE_ORDERLIST_REMOVE_FIRST_OBJ';
export const AUTO_SCHEDULE_SET_WITH_OUT_IDS = 'AUTO_SCHEDULE_SET_WITH_OUT_IDS'

//
export function setWithOutIDs(withOutIds){
    return{
        type:AUTO_SCHEDULE_SET_WITH_OUT_IDS,
        withOutIds
    }
}

//設定現在規劃
export function setNowPlan(day,timeFragmentId,timeFragmentRemain){
    console.log(day,timeFragmentId,timeFragmentRemain)
    return{
        type:AUTO_SCHEDULE_SET_NOW_PLAN,
        day,
        timeFragmentId,
        timeFragmentRemain
    }
}

//選擇並且儲存地點
export function selectAndSavePoint(data,day,timeFragmentId){        
    // console.log(day,timeFragmentId)
    return (dispatch)=>{
        
        let rDistanceValues=[]; //儲存距離結果倒數
        let rDistanceValuesSum = 0; //分母
        data.map((location)=>{
            rDistanceValues.push(1/location.trafic_info_relative_last_select.distance.value);
            rDistanceValuesSum = rDistanceValuesSum + 1/location.trafic_info_relative_last_select.distance.value;
        });
        
        let rDistanceValuesRandomPoints=[]; //隨機點
        let select = Math.random(); //選擇點

        //開始計算隨機點
        let tempRDistanceValue = 0
        rDistanceValues.map((value,index)=>{
            tempRDistanceValue = tempRDistanceValue + value
            rDistanceValuesRandomPoints[index] = tempRDistanceValue/rDistanceValuesSum
        });

        //抽
        let selectPoint = 0;
        for(let i=0;i<rDistanceValuesRandomPoints.length;i++){            
            if(select<rDistanceValuesRandomPoints[i]){
                selectPoint = i
                break;
            }            
        }

        //抽出結果
        console.log(data[selectPoint],rDistanceValuesRandomPoints[selectPoint])        
        
        // day,placeId,placeName,timeFragmentId,trafficAndStayTime,lon,lat
        let { id:placeId, title:placeName,
            trafic_info_relative_last_select,stay_time,
            latitude,longitude
        } =data[selectPoint],
        {duration} = trafic_info_relative_last_select;

        let trafficAndStayTime = parseInt(stay_time) + parseInt(Math.round(duration.value/60))
        
        let point ={
            day,
            placeId,
            placeName,
            timeFragmentId,
            trafficAndStayTime,
            lat:latitude,
            lon:longitude
        }

        console.log(point)

        dispatch({
            type:AUTO_SCHEDULE_SAVE_POINT,
            selectPoint:point,
            nowPlanLon:longitude,
            nowPlanLat:latitude
        });
    }
}

export function rmOrderListFirstOBJ(orderList,orderId = 0){
    return (dispatch)=>{
        let newOrderList = Object.assign([],orderList)
        if(orderId !== 0 ){
            newOrderList = newOrderList.filter((obj,index)=>{            
                return index !== 0
            });
        }
        else{
            newOrderList = newOrderList.filter((obj)=>{            
                return obj.id !== parseInt(orderId)
            });
        }
        dispatch({
            type:AUTO_SCHEDULE_ORDERLIST_REMOVE_FIRST_OBJ,
            orderList:newOrderList
        });
    }
}

//
export function getA2BPlaceInfo(lat_a,lon_a,placeB_id,onDay,timeFragmentId){
    return (dispatch)=>{
        dispatch({type:AUTO_SCHEDULE_GET_A2B_PLACE_INFO})
        axios.post(API_HOST+'/get-a2b-place-info',{
            token:sessionStorage.getItem('user'),
            lat_a,
            lon_a,
            placeB_id
        })
        .then((res)=>{
            console.log(res.data)
            let { data } = res,
            { stay_time,trafic_info_relative_last_select } = data,
            { duration } = trafic_info_relative_last_select;           

            let trafficAndStayTime = parseInt(stay_time) + parseInt(Math.round(duration.value/60))
            
            let point ={
                day:onDay,
                placeId:data.id,
                placeName:data.title,
                timeFragmentId,
                trafficAndStayTime,
                lat:data.latitude,
                lon:data.longitude
            }
            
            dispatch({
                type:AUTO_SCHEDULE_SAVE_POINT,
                selectPoint:point,
                nowPlanLon:data.longitude,
                nowPlanLat:data.latitude
            });
            
        })
        .catch((err)=>{            
            console.log(err)
        })
    }
}


//開始獲取一個地點
export function startSchedule (selfLon,selfLat,day,timeFragmentId,withOutPlaceIds=[],type=1){
    return (dispatch)=>{
        dispatch({type:AUTO_SCHEDULE_START_SCHEDULE})
        axios.post(API_HOST+(type===1?'/get-nearby-place':'/recommend-food'),{
            token:sessionStorage.getItem('user'),
            lon:selfLon,
            lat:selfLat,
            range:5000,
            with_out_place_ids:withOutPlaceIds                        
        })
        .then((res)=>{
            console.log(res)
            dispatch(selectAndSavePoint(res.data,day,timeFragmentId));
        })
        .catch((err)=>{
            console.log(err)
        })        
    }
}

export function setRestaurantFlag(bool){
    return{
        type:AUTO_SCHEDULE_SET_RESTARUANT_FALG,
        needResaurantFlag:bool
    }
}

export function initAutoSchedule(){
    return{
        type:AUTO_SCHEDULE_INIT
    }
}

export function addAlreadySchedule(addValue=1){
    return {
        type:AUTO_SCHEDULE_ADD_ALREADY_SCHEDULE,
        addValue
    }
}

export function scheduleFinish(){
    return {
        type:AUTO_SCHEDULE_FINISH
    }
}