import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {startSchedule,setNowPlan,
    setRestaurantFlag,addAlreadySchedule,
    scheduleFinish,getA2BPlaceInfo,
    rmOrderListFirstOBJ,setWithOutIDs
} from './action.js';
import {saveSchedule} from 'AppModules/travelCardModule/action.js';
import {getTimeFragmentLimitAt,getTimeFragmentStartAt,timeInterval} from 'AppCore/timeTools.js'
import { withRouter } from "react-router-dom";
import './index.css'

class AutoSchedule extends Component {
    constructor(props) {
        super(props);        
        this.countHowManyTimeFragment = this.countHowManyTimeFragment.bind(this)
        this.filterOrderListOnday = this.filterOrderListOnday.bind(this)
    }

    componentDidMount(){        
        let { dispatch,AskTravelInfoReducer } = this.props,
        { inputBundle={},orderList=[] } = AskTravelInfoReducer,
        {startAtLatLon,startAtTimeId=1,startAtTime}=inputBundle;

        //fake
        // orderList = JSON.parse('[{"id":638,"placeName":"ic1aRy72Jv","onDay":"1","timeFragment":"2","placeLat":"24.0937420","placeLon":"120.6713810"},{"id":648,"placeName":"xc8sK1yoxs","onDay":"1","timeFragment":"3","placeLat":"24.2521010","placeLon":"120.6655980"},{"id":612,"placeName":"qjrxdI2by8","onDay":"2","timeFragment":"3","placeLat":"24.0381150","placeLon":"120.9096640"}]');
        // inputBundle = JSON.parse('{"area":{"id":1,"name":"台中","longitude":"120.6736482","latitude":"24.1477358"},"days":"2","isSelectHotel":"false","startAt":"台灣台中東區火車站(東站)","endAt":"台灣台中市烏日站區二路高鐵台中站","orderHotelList":[],"startAtTime":"08:00","endAtTime":"21:30","startAtLatLon":{"lat":24.1373261,"lon":120.68678879999993},"endAtLatLon":{"lat":23.8820205,"lon":120.94549559999996},"startAtTimeId":1,"endAtTimeId":3}')
        // startAtLatLon = inputBundle.startAtLatLon
        // startAtTime = inputBundle.startAtTime

        //排除指定地點
        let withOutIds = [];
        orderList.map((obj)=>{
            withOutIds.push(obj.id)
        })
        console.log(withOutIds)
        dispatch(setWithOutIDs(withOutIds));

        //設定排程標記
        let timeRemain = timeInterval(startAtTime,getTimeFragmentLimitAt(startAtTimeId))
        dispatch(setNowPlan(1,startAtTimeId,timeRemain))

        //取出第一天開始時段指定點
        let firstDayOrderList = this.filterOrderListOnday(orderList,1);
        let firstDayStartFragmentOrderList = this.filterOrderListOnday(orderList,1,startAtTimeId);        

        if(firstDayStartFragmentOrderList.length>0){ //如果第一天開始時段有指定            
            //取得起點到首個指定地點的交通時間&停留時間
            console.log('取得起點到首個指定地點的交通時間&停留時間')            
            dispatch(getA2BPlaceInfo(
                startAtLatLon.lat,
                startAtLatLon.lon,                
                firstDayStartFragmentOrderList[0].id,
                1,
                startAtTimeId
            ))
            dispatch(rmOrderListFirstOBJ(orderList)) //移除第一個指定點然後放入autoSchedule reducer
        }
        else if(firstDayOrderList.length>0){//否則以以第一天第一個指定點(不分時段)
            //以開始點&首個指定進行地點搜尋
            console.log('以開始點&首個指定進行地點搜尋',firstDayOrderList)
            let orderLat = firstDayOrderList[0].placeLat;
            let orderLon = firstDayOrderList[0].placeLon;            
            let middleLat = (parseFloat(orderLat) + parseFloat(startAtLatLon.lat))/2
            let middleLon = (parseFloat(orderLon) + parseFloat(startAtLatLon.lon))/2            
            dispatch(startSchedule(middleLon,middleLat,1,startAtTimeId));
            dispatch(rmOrderListFirstOBJ(orderList)) 
        }
        else{ //第一天沒有任何指定
            console.log('第一天沒有任何指定')            
            dispatch(startSchedule(startAtLatLon.lon,startAtLatLon.lat,1,startAtTimeId));        
        }
    }

    filterOrderListOnday(orderList=[],onDay,timeFragment=0){
        onDay = parseInt(onDay)
        timeFragment = parseInt(timeFragment);
        let newOredrList = Object.assign([],orderList);
        
        //取出onDay全部
        if(timeFragment === 0){
            return newOredrList.filter((place)=>{
                return parseInt(place.onDay) === onDay
            })
        }
        else{ //取出onDay指定時段
            return newOredrList.filter((place)=>{
                return parseInt(place.onDay) === onDay 
                    && parseInt(place.timeFragment) === timeFragment
            })
        }
    }

    componentWillReceiveProps(nextProps){
        console.log('next',nextProps);
        let  {nowPlanLon,nowPlanLat,selectPlaceIds,
            nowPlanTimeFragmentId,nowPlanDay,nowPlanTimeFragmentRemain,
            needResaurantFlag,scheduleIsFinish,
            orderList
        } = nextProps.AutoModeReducer

        if(scheduleIsFinish){
            return
        }

        let { inputBundle={} } = this.props.AskTravelInfoReducer
        let { history } = this.props
        
        //fake
        // inputBundle = JSON.parse('{"area":{"id":1,"name":"台中","longitude":"120.6736482","latitude":"24.1477358"},"days":"2","isSelectHotel":"false","startAt":"台灣台中東區火車站(東站)","endAt":"台灣台中市烏日站區二路高鐵台中站","orderHotelList":[],"startAtTime":"08:00","endAtTime":"21:30","startAtLatLon":{"lat":24.1373261,"lon":120.68678879999993},"endAtLatLon":{"lat":23.8820205,"lon":120.94549559999996},"startAtTimeId":1,"endAtTimeId":3}')
        
        let { endAtTimeId,days,endAtTime } =inputBundle;
        endAtTimeId = parseInt(endAtTimeId)
        days = parseInt(days)
        let {dispatch} = this.props

        let judgeLastTimefragment = (switchTimeFragmentId=nowPlanTimeFragmentId)=>{
            if(nowPlanDay === days && switchTimeFragmentId === endAtTimeId){
                return true
            }
            return false;
        }

        //如果剩餘時間>=30分鐘繼續規劃
        console.log('現在規劃:'+nowPlanDay,'現在時段:'+nowPlanTimeFragmentId,'剩餘時間:'+nowPlanTimeFragmentRemain)
        console.log('指定:',orderList)
        let nowDayAndFragmentOrderList = this.filterOrderListOnday(orderList,nowPlanDay,nowPlanTimeFragmentId);
        let nowDayOrderList = this.filterOrderListOnday(orderList,nowPlanDay,0);
        if(nowDayAndFragmentOrderList.length>0){ //如果目前天&時段有指定的地點
            dispatch(getA2BPlaceInfo(
                nowPlanLat,
                nowPlanLon,                
                nowDayAndFragmentOrderList[0].id,
                nowPlanDay,
                nowPlanTimeFragmentId
            ))
            dispatch(rmOrderListFirstOBJ(orderList,nowDayAndFragmentOrderList[0].id))
        }
        else if(nowDayOrderList>0 && nowPlanTimeFragmentRemain>=30 && needResaurantFlag !== true){ //如果目前天有指定(不分時段)
            let orderLat = nowDayOrderList[0].placeLat;
            let orderLon = nowDayOrderList[0].placeLon;            
            let middleLat = (parseFloat(orderLat) + parseFloat(nowPlanLat))/2
            let middleLon = (parseFloat(orderLon) + parseFloat(nowPlanLon))/2            
            dispatch(startSchedule(middleLon,middleLat,nowPlanDay,nowPlanTimeFragmentId));
            dispatch(rmOrderListFirstOBJ(orderList,nowDayOrderList[0]))
        }
        else if(nowPlanTimeFragmentRemain>=30){ //如果沒有但是還有剩餘時間
            dispatch(startSchedule(nowPlanLon,nowPlanLat,nowPlanDay,nowPlanTimeFragmentId,selectPlaceIds,needResaurantFlag===true?2:1))
        }
        else{                        
            // 切換時區或是換天
            let fragmentStart;
            if(nowPlanDay !== days || nowPlanTimeFragmentId !== endAtTimeId){ //如果還沒有規劃完成
                if(nowPlanTimeFragmentId === 1){
                    dispatch(setRestaurantFlag(true))
                    if(!judgeLastTimefragment(2)){
                        dispatch(setNowPlan(undefined,2,240))
                    }
                    else{ //如果下一區間是最後                        
                        fragmentStart =  getTimeFragmentStartAt(2);
                        dispatch(setNowPlan(undefined,2,timeInterval(fragmentStart,endAtTime)))
                    }                    
                }
                else if(nowPlanTimeFragmentId === 2){
                    dispatch(setRestaurantFlag(true))
                    if(!judgeLastTimefragment(3)){
                        dispatch(setNowPlan(undefined,3,240))
                    }
                    else{ //如果下一區間是最後
                        fragmentStart =  getTimeFragmentStartAt(3);
                        dispatch(setNowPlan(undefined,3,timeInterval(fragmentStart,endAtTime)))
                    }                    
                }
                else if(nowPlanTimeFragmentId === 3){
                    dispatch(setRestaurantFlag(true))
                    if(!judgeLastTimefragment(1)){
                        dispatch(setNowPlan(nowPlanDay+1,1,240))
                    }
                    else{ //如果下一區間是最後
                        fragmentStart =  getTimeFragmentStartAt(1);
                        dispatch(setNowPlan(nowPlanDay+1,1,timeInterval(fragmentStart,endAtTime)))
                    }                                        
                }
                else{
                    console.error('切換時區或天發生錯誤');
                }
            }
            else{//規劃完了
                console.log('規劃完成')
                dispatch(addAlreadySchedule())
                dispatch(scheduleFinish())
                dispatch(saveSchedule({
                    travelCardModeReducer:nextProps.AutoModeReducer,
                    askTravelInfoReducer:this.props.AskTravelInfoReducer
                }))
                .then(function(res){
                    history.push('/schedule-result?sid='+res);
                });
                return
            }
        }
    }
 
    //計算總共多少時間區間
    countHowManyTimeFragment(){
        let { AskTravelInfoReducer } = this.props,
        { inputBundle={} } = AskTravelInfoReducer;

        //fake
        // inputBundle = JSON.parse('{"area":{"id":1,"name":"台中","longitude":"120.6736482","latitude":"24.1477358"},"days":"2","isSelectHotel":"false","startAt":"台灣台中東區火車站(東站)","endAt":"台灣台中市烏日站區二路高鐵台中站","orderHotelList":[],"startAtTime":"08:00","endAtTime":"21:30","startAtLatLon":{"lat":24.1373261,"lon":120.68678879999993},"endAtLatLon":{"lat":23.8820205,"lon":120.94549559999996},"startAtTimeId":1,"endAtTimeId":3}')
        //

        let { days,endAtTimeId=0,startAtTimeId=0 } = inputBundle;
        startAtTimeId = parseInt(startAtTimeId)
        endAtTimeId = parseInt(endAtTimeId)
        days = parseInt(days)
        let totalTimeFragment = 0;
        for(let i=1;i<=days;i++){
            let offset = 0
            if(i===1){
                offset = offset + (startAtTimeId-1)*-1
            }
            if(i===days){
                offset = offset +
                 endAtTimeId-3                
            }

            // console.log(i,days)
            totalTimeFragment = totalTimeFragment + 3 + offset
        }

        return totalTimeFragment
    }

    render() {
        let { AskTravelInfoReducer,AutoModeReducer } = this.props,
        { inputBundle } = AskTravelInfoReducer,
        { nowPlanDay,nowPlanTimeFragmentId,alreadyShceduleTimeFragment } =AutoModeReducer,
        { days } = inputBundle;
        //nowPlanTimeFragmentRemain
        console.log(AskTravelInfoReducer,AutoModeReducer);
        let totalTimeFragment = this.countHowManyTimeFragment();

        let progressStatus = Math.round(alreadyShceduleTimeFragment/totalTimeFragment*100);
        progressStatus>100?'100':String(progressStatus)
        
        return (
            <div id="autoSchedule">
            {/* order mode */}
            <br/>
                <div className="progerss-info">
                    <p>進度:{progressStatus}%</p>
                    <p>現在規劃(天):{nowPlanDay}/{days}</p>
                    <p>現在規劃時段:{nowPlanTimeFragmentId}</p>
                    <p>已經完成規劃時段:{alreadyShceduleTimeFragment}/{totalTimeFragment}</p>                    
                </div>

                {/* <div className="progerss-info">
                    <p>進度:{alreadyShceduleTimeFragment/totalTimeFragment}</p>
                    <p>旅遊天數:{days}</p>
                    <p>總時段數:{totalTimeFragment}</p>
                    <hr/>
                    <p>現在規劃:{nowPlanDay}/{days}</p>
                    <p>現在規劃時段:{nowPlanTimeFragmentId}</p>
                    <p>現在時段剩餘:{nowPlanTimeFragmentRemain}</p>
                    <p>已經規劃時段數:{alreadyShceduleTimeFragment}</p>
                </div> */}

               <div className="progress">
                    <div 
                        key={progressStatus}
                        className="progress-bar progress-bar-striped progress-bar-animated" 
                        role="progressbar" 
                        aria-valuemin="0" 
                        aria-valuemax="100" 
                        aria-valuenow={progressStatus}
                        style={{'width': progressStatus + '%'}}
                    />
                </div>
            </div>
        );
    }
}

let mapStatetoProps=(state)=>{
    return{
        AskTravelInfoReducer:state.AskTravelInfoReducer,
        AutoModeReducer:state.AutoModeReducer
    }
}

AutoSchedule.propTypes={
    AskTravelInfoReducer:PropTypes.object,
    AutoModeReducer:PropTypes.object,
    dispatch:PropTypes.func,
    history:PropTypes.func
}

export default withRouter(connect(mapStatetoProps)(AutoSchedule));
