import { 
    MAIN_MODULE_SET_APP_LOADING,
    MAIN_MODULE_SET_DIALOG_MSG
} from './action'

//reducer
const initstate = ()=>{
    return{
        loadingState:false,
        dialogMsg:{}
    };
}

export default (state=initstate(),action)=>{
    switch (action.type) {
        case MAIN_MODULE_SET_APP_LOADING:
            return Object.assign({},state,{
                loadingState:action.loadingState
            })
        case MAIN_MODULE_SET_DIALOG_MSG:
            return Object.assign({},state,{
                dialogMsg:action.dialogMsg
            })
        default:
            return state
    }
}