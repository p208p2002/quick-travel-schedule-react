import React, { Component } from 'react';
import './index.scss';
import { connect } from 'react-redux'
import PropTypes from 'prop-types';

class Index extends Component {    
    render() {
        let { loadingState:show } = this.props.mainReducer;        
        return ( 
            <div id="loading-view">
                {show?(
                    <div className="lmask"/>
                ):(
                    null
                )}
            </div>
        );
    }
}

Index.propTypes={
    mainReducer:PropTypes.object
}

let mapStateToProps = (state)=>{
    return{
        mainReducer: state.MainReducer
    }
}
 
export default connect(mapStateToProps)(Index);