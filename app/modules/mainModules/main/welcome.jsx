import React, { Component } from 'react';
import { Link } from 'react-router-dom'

class View extends Component {
    render() { 
        return ( 
            <div className="container">
            <br/>
                <div className="card">
                    <div className="card-body">
                        <div className="card-title text-center"><h2>歡迎使用 QTS</h2></div>
                        <h6 className="card-subtitle mb-2 text-muted text-center">簡單兩步驟，輕鬆建立旅遊行程</h6>
                        <br/>
                        <div className="text-center">                            
                            <Link className="btn btn-success btn-lg" to="/ask-travel-info">立即開始</Link>
                        </div>
                    </div>
                </div>
        </div>
         )
    }
}
 
export default View;
