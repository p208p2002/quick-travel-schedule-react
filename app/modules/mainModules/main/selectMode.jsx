import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class View extends Component {
    render() { 
        return ( 
            <div className="container">
                <br/>
                <h1 className="text-center">選擇模式</h1>
                <hr/>

                <Link to="#">
                <div className="card text-white bg-info mb-3">
                    <div className="card-body  text-center">
                    <h4 className="card-title">演算法排定旅遊行程</h4>
                    <p className="card-text">全自動化排定旅遊行程，您只需等待結果即可!</p>
                    </div>
                </div>
                </Link>

                <Link to="travel-scheduling">
                <div className="card text-black bg-light mb-3">
                    <div className="card-body  text-center">
                    <h4 className="card-title">演算法推薦旅遊行程</h4>
                    <p className="card-text">由演算法推薦旅遊地點，您來選擇。可供地點比較。</p>
                    </div>
                </div>
                </Link>

            </div>
        )
    }
}
 
export default View;