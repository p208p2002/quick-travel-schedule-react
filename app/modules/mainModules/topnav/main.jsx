import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './main.css';
import { withRouter } from "react-router-dom";
import PropTypes from 'prop-types';

class View extends Component {
    componentDidMount(){
        // if(!sessionStorage.getItem('user')){
        //     let {push}=this.props.history;
        //     push('/');
        // }
    }
    render() { 
        return ( 
            <div id="header">
                <nav className="navbar navbar-light bg-dark-blue justify-content-center">
                    <Link to="member-index">
                        <span className="navbar-brand mb-0 h1 text-white">Quick travel schedule</span>
                    </Link>                
                </nav>
                <div className="bg-dark text-gray">
                    <div className="d-flex justify-content-center">                        
                        <div className="p-2"><Link to="/ask-travel-info">規劃旅遊</Link></div>
                        <div className="p-2"><Link to="/my-travel">我的旅遊</Link></div>                        
                        <div className="p-2"><Link to="#">常見問題</Link></div>
                        <div className="p-2">
                            <a className="fake-a"
                                onClick={()=>{
                                    sessionStorage.clear();
                                    this.props.history.push('/')
                                }}>
                                登出
                            </a>
                        </div>
                    </div>
                </div>
            </div>
         )
    }
}

View.propTypes={
    history:PropTypes.object
}

export default withRouter((View));