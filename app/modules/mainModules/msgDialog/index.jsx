import React, { Component } from 'react';
import './index.css';
import { connect } from 'react-redux';
import { setDialogMsg } from '../action.js';
import PropTypes from 'prop-types';

class Index extends Component { 
    render() {
        let {MainReducer}=this.props;
        let { dialogMsg } = MainReducer
        let {msgTitle,msgContext} = dialogMsg;        
        if(!msgTitle || !msgContext){
            dialogMsg = false;
        }
        return (
           <div>
                {dialogMsg?
                    <div id="dialog">
                    <div className="dialog-block bg-gray">
                        <div className="dialog-title bg-dark-blue text-white text-center">
                            <h3>{msgTitle}</h3>                        
                        </div>                
                    <div className="dialog-body">
                        <div className="dialog-text">
                        {msgContext}
                        </div>
                    </div>
                        <button 
                            type="button" 
                            className="btn dialog-btn"
                            onClick={()=>{
                                let { dispatch } = this.props;
                                dispatch(setDialogMsg(undefined,undefined))
                            }}
                        >
                                <b>確認</b>
                        </button>
                    </div>
                    </div>
                    :
                    null
                }
           </div>
        )
    }
}

let mapStateToProps = (state)=>{
    return{
        MainReducer:state.MainReducer
    }
}

Index.propTypes = {
    MainReducer:PropTypes.object,
    dispatch:PropTypes.func
}

export default connect(mapStateToProps)(Index);