import React, { Component } from 'react';
import './index.css'
import fbIcon from 'AppSrc/img/003-facebook-logo-button.png';
import igIcon from 'AppSrc/img/002-instagram-logo.png';
import tiwtterIcon from 'AppSrc/img/001-twitter-logo-button.png';

class Footer extends Component {   
    render() { 
        return (
            <footer id="footer" className="bg-dark-blue text-light-gray">
                <div className="container">
                    <div className="text-center">
                        <img className="icon" src={fbIcon} alt=""/>
                        <img className="icon" src={igIcon} alt=""/>
                        <img className="icon" src={tiwtterIcon} alt=""/>
                    </div>                    
                    <br/>
                    <div className="row text-center">
                        <div className="col-md col-12">
                            <span className="text-default"><b>相關網站</b></span>
                            <hr/>
                            <ul>
                                <li>QTS-旅遊規劃</li>
                                <li>QTS-旅遊指南</li>                                
                            </ul>
                            <br className="d-md-none d-lg-none d-xl-none"/>
                        </div>
                        <div className="col-md col-12">
                            <span className="text-default"><b>聯絡我們</b></span>
                            <hr/>
                            <ul>
                                <li>TEL: 02-12345678</li>
                                <li>E-mail: qtsproject@web.com.tw</li>
                            </ul>
                            <br className="d-md-none d-lg-none d-xl-none"/>
                        </div>
                        <div className="col-md col-12">
                            <span className="text-default"><b>合作提案</b></span>
                            <hr/>
                            <ul>
                                <li>TEL: 02-12345678</li>
                                <li>E-mail: qtsproject@web.com.tw</li>
                            </ul>
                            <br className="d-md-none d-lg-none d-xl-none"/>
                        </div>
                    </div> 
                    <hr/>
                    <div className="container-fluid text-center">
                        <span>2018 © QTS team版權所有</span>
                        <br/>                        
                        <br/>
                    </div>                    
                </div>
            </footer>
        );
    }
}
 
export default Footer;