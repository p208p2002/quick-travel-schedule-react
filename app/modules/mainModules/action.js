export const MAIN_MODULE_SET_APP_LOADING = 'MAIN_MODULE_SET_APP_LOADING'
export const MAIN_MODULE_SET_DIALOG_MSG = 'MAIN_MODULE_SET_DIALOG_MSG'

export function setAppLoadingState(bool){
    return{
        type:MAIN_MODULE_SET_APP_LOADING,
        loadingState:bool
    }
}

export function setDialogMsg(msgTitle,msgContext){
    return{
        type:MAIN_MODULE_SET_DIALOG_MSG,
        dialogMsg:{msgTitle,msgContext}
    }
}