import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import {Link} from 'react-router-dom'
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux'

import { 
    pushFormInputToStore, 
    getAvaliableArea,
} from './action'
import { setNowTimeFragment } from 'AppModules/travelCardModule/action.js'

import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import './main.css';
import RowOfNewHotel from './rowOfNewHotel.jsx';
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Marker,
} from "react-google-maps";
import 'App/src/styles/rc-time-picker/index.less'
import TimePicker from 'rc-time-picker';
import moment from 'moment';
import {singleTimeJudge,singleTimeJudgeId} from 'AppCore/timeTools.js'
import { setDialogMsg } from 'App/modules/mainModules/action.js';
import { initAutoSchedule } from 'App/modules/autoScheduleModule/action.js'
import { initTravelCard } from 'App/modules/travelCardModule/action.js'


class View extends Component {
    constructor(props) {
        super(props)

        this.state = {
            needHotel: false,
            isSelectHotel: 'false',
            startAt: '',
            startAtTime:'08:00',
            endAt: '',
            endAtTime:'21:30',
            orderHotelList: [],
            hotelOrder: '',
            orderHotelDefaultDay: '1',
            //google map
            mapLat: 23.903687,
            mapLon: 121.079370,
            mapZoom: 8,
            startAtLatLon:undefined,
            endAtLatLon:undefined
        }
        this.dispatchInput = this.dispatchInput.bind(this)
        this.travelDaysOnchange = this.travelDaysOnchange.bind(this)
        this.isSelectHotelOnchange = this.isSelectHotelOnchange.bind(this)
        this.handleFormSubmit = this.handleFormSubmit.bind(this)
        this.setOrderHotel = this.setOrderHotel.bind(this);
        this.selectAreaOnChange = this.selectAreaOnChange.bind(this);
        this.startAtTimePickerOnchange = this.startAtTimePickerOnchange.bind(this);
        this.endAtTimePickerOnchange = this.endAtTimePickerOnchange.bind(this);
        //
        this.startAtonChange = (address) => this.setState({ startAt: address })
        this.endAtonChange = (address) => this.setState({ endAt: address })
        this.hotelOrderonChange = (address) => this.setState({ hotelOrder: address }) 
        this.handleInputChange = this.handleInputChange.bind(this);
        this.getGeocodeByAddress = this.getGeocodeByAddress.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;        
        this.setState({
          [name]: value
        });
      }

    startAtTimePickerOnchange(value){
        if(value===null){            
            return;
        }
        console.log(value&&value.format('HH:mm'));
        this.setState({
            startAtTime:value.format('HH:mm')
        })
    }

    endAtTimePickerOnchange(value){
        if(value===null){            
            return;
        }
        console.log(value&&value.format('HH:mm'));
        this.setState({
            endAtTime:value.format('HH:mm')
        })
    }

    selectAreaOnChange() {
        let obj = JSON.parse(this.area.value)
        console.log(obj)
        this.setState({
            mapLat:parseFloat(obj.latitude),
            mapLon:parseFloat(obj.longitude),
            mapZoom:10
        })
    }

    handleFormSubmit(address, caseState, resAddress) {
        console.log(address, caseState, resAddress)
        switch (caseState) {
            case 'startAt':
                console.log('in')
                this.setState({
                    startAt: resAddress                    
                })
                break;
            case 'endAt':
                this.setState({
                    endAt: resAddress
                })
                break;
            case 'hotelOrder':
                this.setState({
                    hotelOrder: resAddress
                })
                break;

            default:
                break;
        }

        geocodeByAddress(address)
            .then(results => getLatLng(results[0]))
            .then((latLng) =>{
                console.log('Success', latLng)
                let name = caseState+'LatLon'
                this.setState({
                    [name]:{
                        lat:latLng.lat,
                        lon:latLng.lng
                    }
                })
            })
            .catch(error => console.error('Error', error))
    }


    getGeocodeByAddress(address,saveKey){
        return geocodeByAddress(address)
                .then(results => getLatLng(results[0]))
                .then((latLng) =>{
                    console.log('Success', latLng)
                    let name = saveKey+'LatLon'
                    this.setState({
                        [name]:{
                            lat:latLng.lat,
                            lon:latLng.lng
                        }
                    })
                })
                .catch(()=>{
                    this.props.dispatch(setDialogMsg('錯誤','沒有被法解析地點，請確認輸入或點擊搜尋建議選項'));
                })       
    }
    

    componentDidMount() {
        let { dispatch } = this.props
        dispatch(initAutoSchedule())
        dispatch(initTravelCard())
        dispatch(getAvaliableArea())        
    }

    isSelectHotelOnchange() {
        let isSelectHotel = this.isSelectHotel.value
        this.setState({
            isSelectHotel
        });
    }

    travelDaysOnchange() {
        let days = this.days.value
        if (days > 1) {
            // this.setState({
            //     needHotel: true
            // })
        }
        else {
            this.setState({
                needHotel: false
            })
        }
    }

    setOrderHotel() {
        console.log('set');
        // console.log(document.getElementById('oredrHotelSelect').value);
        let orderHotelList = this.state.orderHotelList;
        let index = (document.getElementById('oredrHotelSelect').value - 1);
        orderHotelList[index] = this.state.hotelOrder;
        let orderHotelDefaultDay = document.getElementById('oredrHotelSelect').value;
        orderHotelDefaultDay++;
        orderHotelDefaultDay = orderHotelDefaultDay.toString();
        console.log('set', orderHotelDefaultDay)
        this.setState({
            orderHotelList,
            hotelOrder: '',
            orderHotelDefaultDay
        });
    }

    dispatchInput() {
        let area = this.area.value
        let days = this.days.value
        let isSelectHotel = (this.isSelectHotel ? (this.isSelectHotel.value) : ('false'))
        let startAt = this.startAt.value
        let endAt = this.endAt.value
        let orderHotelList = this.state.orderHotelList;
        let startAtTime = this.state.startAtTime;
        let endAtTime = this.state.endAtTime;
        let {startAtLatLon,endAtLatLon} = this.state;        
        console.log(startAtLatLon,endAtLatLon);
        
        if(days === ''){
            return Promise.reject('請選擇旅遊天數')
        }

        if(!startAtLatLon || !endAtLatLon){
            return Promise.reject('起始日或結束日的地點沒辦法被解析，請確認輸入或點擊搜尋建議選項')
        }

        let startAtTimeId = singleTimeJudgeId(startAtTime);
        let endAtTimeId = singleTimeJudgeId(endAtTime);
        // console.log(startAtTimeId,endAtTimeId);

        let inputBundle = {
            area,
            days,
            isSelectHotel,
            startAt,
            endAt,
            orderHotelList,
            startAtTime,
            endAtTime,
            startAtLatLon,
            endAtLatLon,
            startAtTimeId,
            endAtTimeId
        }

        // console.log(inputBundle)

        let { dispatch} = this.props
        dispatch(pushFormInputToStore(inputBundle))

        //
        let timeFragment = singleTimeJudge(startAtTime)
        dispatch(setNowTimeFragment(timeFragment))

        return Promise.resolve('resolve');
    }

    render() {
        // withGoogleMap((props)
        const MapWithAMarker = withScriptjs(withGoogleMap(() =>
            <GoogleMap
                defaultZoom={this.state.mapZoom}
                defaultCenter={{ lat: this.state.mapLat, lng: this.state.mapLon }}
            >
                <Marker
                    position={{ lat: this.state.mapLat, lng: this.state.mapLon }}
                />
            </GoogleMap>
        ));

        const cssClasses = {
            root: '',
            input: 'form-control',
            autocompleteContainer: 'Demo__autocomplete-container'
        };

        const cssClasses2 = {
            root: 'form-group width-60 input-ab',
            input: 'form-control',
            autocompleteContainer: 'Demo__autocomplete-container'
        };
       
        const RowOfneedHotel = () => {
            return (
                <div className="form-group">
                    <label className="col-form-label" >是否已選定住宿?</label>
                    <select
                        defaultValue={this.state.isSelectHotel}
                        onChange={() => { this.isSelectHotelOnchange() }}
                        ref={(input) => { this.isSelectHotel = input }}
                        className="form-control"
                    >
                        <option value="false">否</option>
                        <option value="true">是</option>
                    </select>
                    <br />
                </div>
            )
        }

        let avaliableArea = this.props.avaliableArea
        let {dispatch} = this.props;
        let {push} = this.props.history;

        return (
            <div id="askInfo" className="row">
                <div className="col d-none d-md-block">
                    <br />
                    <MapWithAMarker
                        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAidB8XIVQpo0G6c4kuQ31wOInYlkMuovM&v=3.exp&libraries=geometry,drawing,places"
                        loadingElement={<div style={{ height: `100%` }} />}
                        containerElement={<div style={{ height: `650px` }} />}
                        mapElement={<div style={{ height: `100%` }} />}
                    />
                </div>
                <div className="col">
                    <div className="container">
                        <br />
                        <h1 className="text-center">請告訴我們必要的資訊</h1>
                        <hr />
                        <form>
                            <div className="form-group">
                                <label className="col-form-label" >前往的旅遊地區?</label>

                                <select
                                    ref={(input) => { this.area = input }}
                                    className="form-control"
                                    onChange={() => { this.selectAreaOnChange() }}
                                >
                                    {avaliableArea.map((area, index) => {
                                        return (
                                            <option key={index} value={JSON.stringify(area)}>{area.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-group">
                                <label className="col-form-label" >旅遊的天數?</label>
                                <input
                                    ref={(input) => { this.days = input }}
                                    onChange={(e) => {
                                        this.handleInputChange(e)
                                        this.travelDaysOnchange()                                                                    
                                    }}
                                    type="number"
                                    className="form-control"
                                    value={Math.abs(this.state.travelDay)===0?'':Math.abs(this.state.travelDay)}
                                    name="travelDay"
                                />
                            </div>
                            {this.state.needHotel ? (
                                <div>
                                    <RowOfneedHotel />
                                    {this.state.isSelectHotel === 'true' ? (
                                        <div className="order-hotel-form">
                                            <small>告訴我們您的住宿點</small>
                                            <div className="auto-complete form-group">
                                                <RowOfNewHotel
                                                    optionNum={this.days.value}
                                                    defaultDay={this.state.orderHotelDefaultDay}
                                                />
                                                <PlacesAutocomplete
                                                    inputProps={{
                                                        value: this.state.hotelOrder,
                                                        onChange: this.hotelOrderonChange,
                                                    }}
                                                    classNames={cssClasses2}
                                                    onSelect={(address) => { this.handleFormSubmit(this.state.hotelOrder, 'hotelOrder', address) }}
                                                    onEnterKeyDown={(address) => { this.handleFormSubmit(this.state.hotelOrder, 'hotelOrder', address) }}
                                                />
                                                <button
                                                    type="button"
                                                    onClick={() => { this.setOrderHotel() }}
                                                    className="btn btn-success btn-ab"
                                                >
                                                    設定
                                    </button>
                                            </div>
                                            {/*  */}
                                            <br />
                                            <ul>
                                                {this.state.orderHotelList.map((item, index) =>
                                                    <li key={index}>第{index + 1}天晚上:{item}</li>
                                                )}
                                            </ul>
                                        </div>
                                    ) : (
                                            <small className="order-hotel-note">如未選定住宿，後續將由演算法推薦住宿給您</small>
                                        )}
                                </div>
                            ) : (
                                    ''
                                )}

                            <div className="form-group">
                                <label className="col-form-label" >您將在旅遊地區的何處<b>開始</b>旅程?&nbsp;<b>(起始日)</b></label>
                                <div className="row">
                                    <div className="col">                                        
                                        <TimePicker 
                                            value={moment(this.state.startAtTime,"HH:mm")}
                                            onChange={(time)=>this.startAtTimePickerOnchange(time)}
                                            showSecond={false}
                                            use12Hours={false}                                            
                                        />
                                    </div>
                                    <span style={{lineHeight:'35px'}}>抵達</span>
                                    <div className="col-7">
                                            <PlacesAutocomplete
                                                inputProps={{
                                                    value: this.state.startAt,
                                                    onChange: this.startAtonChange,
                                                    onBlur:()=>{
                                                        this.getGeocodeByAddress(this.state.startAt,'startAt')
                                                    },
                                                    ref: (input) => { this.startAt = input },
                                                }}
                                                classNames={cssClasses}
                                                onSelect={(address) => { this.handleFormSubmit(this.state.startAt, 'startAt', address) }}
                                                onEnterKeyDown={(address) => { this.handleFormSubmit(this.state.startAt, 'startAt', address) }}
                                            />                                
                                    </div>
                                </div>                                
                            </div>

                            <div className="form-group-prepend">
                                <label className="col-form-label" >您將在旅遊地區的何處<b>結束</b>旅程?&nbsp;<b>(結束日)</b></label>
                                <div className="row">
                                    <div className="col">                                                 
                                        <TimePicker 
                                            value={moment(this.state.endAtTime,"HH:mm")}
                                            onChange={(time)=>this.endAtTimePickerOnchange(time)}
                                            showSecond={false}
                                            use12Hours={false}
                                        />
                                    </div>
                                    <span style={{lineHeight:'35px'}}>抵達</span>
                                    <div className="col-7">                                      
                                        <PlacesAutocomplete
                                            inputProps={{
                                                value: this.state.endAt,
                                                onChange: this.endAtonChange,
                                                ref: (input) => { this.endAt = input },
                                                onBlur:()=>{
                                                    this.getGeocodeByAddress(this.state.startAt,'endAt')
                                                },
                                            }}
                                            classNames={cssClasses}
                                            onSelect={(address) => { this.handleFormSubmit(this.state.endAt, 'endAt', address) }}
                                            onEnterKeyDown={(address) => { this.handleFormSubmit(this.state.endAt, 'endAt', address) }}
                                        />
                                    </div>
                                </div>                                
                            </div>
                            <hr/>
                            <div className="text-center">
                                <div className="row">
                                <div className="col">                                        
                                        <button
                                            type="button"
                                            className="btn btn-success btn-lg width-100"
                                            onClick={() => {
                                                this.dispatchInput()                                                
                                                .then(function(res){
                                                    console.log(res)        
                                                    push("/ask-travel-info-auto-mode")
                                                })
                                                .catch(function(err){
                                                    console.log(err)
                                                    dispatch(setDialogMsg('錯誤',err));
                                                })     
                                            }}
                                        >
                                            使用全自動排程
                                        </button>                                        
                                    </div>
                                                                        
                                    <div className="col">                                        
                                        <button
                                            type="button"
                                            className="btn btn-info btn-lg width-100"
                                            onClick={() => { 
                                                this.dispatchInput()
                                                .then(function(res){
                                                    console.log(res)        
                                                    push("/travel-scheduling")
                                                })
                                                .catch(function(err){
                                                    console.log(err)
                                                    dispatch(setDialogMsg('錯誤',err));
                                                })                                                
                                            }}
                                        >
                                            使用推薦排程
                                        </button>
                                    </div>
                                </div>
                                {/* <hr/>
                                <button
                                    type="button"
                                    className="btn btn-secondary btn-lg width-100"
                                    onClick={() => { 
                                        this.dispatchInput()                                            
                                        this.props.history.push("/ask-travel-info-auto-mode")
                                    }}
                                >
                                    進階客製選項
                                </button> */}
                                
                            </div>
                        </form>
                    </div>
                </div>                                
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        avaliableArea: state.AskTravelInfoReducer.area,
        state:state
    }
}

View.propTypes = {
    dispatch: PropTypes.func,
    avaliableArea: PropTypes.array,
    history: PropTypes.object
}

export default withRouter(connect(mapStateToProps)(View));