import {ASKTRAVELINFO_DISPATCH_FORM_INPUT,
        ASKTRAVELINFO_DISPATCH_FROM2_INPUT,
        ASKTRAVELINFO_GET_AVAILABLE_AREA,
        ASKTRAVELINFO_ORDER_LOCATION,
        ASKTRAVELINFO_REPLACE_REDUCER,
        ASKTRAVELINFO_PLACES_POLLING
    } from './action.js'

//reducer
const initstate = ()=>{
    // let fakeInputBundle = '{"area":{"id":1, "name":"台中","longitude":"120.6736482","latitude":"24.1477358"},"days":"3","isSelectHotel":"false","startAt":"台中火車站","endAt":"台中烏日高鐵站","orderHotelList":[],"startAtTime":"08:00","endAtTime":"21:30"}';
    return{
        area:[],
        // inputBundle:JSON.parse(fakeInputBundle), //{}
        inputBundle:{},
        customizeList:{},
        autocompleteDatas:[]
    };
}

export default (state=initstate(),action)=>{
    switch (action.type) {
        case ASKTRAVELINFO_DISPATCH_FORM_INPUT:            
            return Object.assign({},state,{
                inputBundle:action.inputBundle
            });
        
        case ASKTRAVELINFO_DISPATCH_FROM2_INPUT:
            return Object.assign({},state,{
                customizeList:action.customizeList
            })
            
        case ASKTRAVELINFO_GET_AVAILABLE_AREA:
            return Object.assign({},state,{
                area:action.area
            });

        case ASKTRAVELINFO_ORDER_LOCATION:
            return Object.assign({},state,{
                orderList:action.orderList
            });
        
        case ASKTRAVELINFO_REPLACE_REDUCER:
            return Object.assign({},action.reducer);

        case ASKTRAVELINFO_PLACES_POLLING:
            return Object.assign({},state,{
                autocompleteDatas:action.autocompleteDatas
            })

        default:
            return state;
    }
}



