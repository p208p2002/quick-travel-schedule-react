import React, { Component } from 'react';
import './askAuto.css'
import { connect } from 'react-redux'
import { pollingPlaces,pushOrderLocationToStore } from './action.js';
import PropTypes from 'prop-types';
import { setDialogMsg } from 'App/modules/mainModules/action.js';
import { timeFragmentIdToTag } from 'AppCore/timeTools.js'
import { withRouter } from "react-router-dom";

class AskAuto extends Component {
    constructor(props) {
        super(props);
        this.state = {
            placeName:'',
            showAutocomplete:true,
            selectFromAutocomplete:false,
            orderPlaces:[]  ,
            whatday:'0',
            whatTimeFragment:'0'
        }
        this.autocompleteForm = this.autocompleteForm.bind(this);
        this.addPlace = this.addPlace.bind(this);
        this.handleInputOnChange = this.handleInputOnChange.bind(this);
        this.onDaySelectOption = this.onDaySelectOption.bind(this);
        this.delPlace = this.delPlace.bind(this);
        this.startAutoSchedule = this.startAutoSchedule.bind(this);
        this.orderForm = this.orderForm.bind(this);
    }

    orderForm(){
        let { orderPlaces } = this.state;
        return(<div>
            {orderPlaces.length === 0?
                            <div className="w-100 text-center">
                                <small>沒有指定的地點</small>
                            </div>
                            :
                            null
                        }
                        {orderPlaces.map((place,index)=>{
                            return(
                                <div key={index} className="row order-places">                                    
                                    <span>預定前往
                                        &nbsp;<b>{place.placeName}</b>&nbsp;
                                        在第&nbsp;{place.onDay}&nbsp;天
                                        &nbsp;{timeFragmentIdToTag(place.timeFragment)}&nbsp;
                                        &nbsp;
                                        <button 
                                            className="btn btn-danger btn-sm"
                                            type="button"
                                            onClick={()=>{
                                                this.delPlace(place.id)
                                            }}
                                        >
                                            刪除
                                        </button>
                                    </span>
                                    <br/>  
                                    <br/>                                  
                                </div>                                
                            )
                        })}
        </div>)
    }

    startAutoSchedule(type=1){
        let { push } = this.props.history
        if(type===1){
            push('/auto-scheduling')
        }
        else{
            push('/order-auto-scheduling')
        }
    }

    onDaySelectOption(days=0){
        days = parseInt(days)
        if(days === 0 ){
            return
        }
        let options = []
        for(var i=0;i<days;i++){
            options.push(
                <option key={i} value={i+1}>{i+1}</option>
            )
        }
        return options
    }

    handleInputOnChange(e){        
        this.setState({
            [e.target.name]:e.target.value
        })
    }

    delPlace(id){
        let { orderPlaces } = this.state
        orderPlaces = orderPlaces.filter((place)=>{            
            return place.id !== id
        })
        this.setState({
            orderPlaces
        })
    }

    addPlace(){
        let { dispatch } = this.props
        let { placeName,whatday,whatTimeFragment,orderPlaces,placeId,placeLat,placeLon } = this.state
        
        if(placeName === '' || parseInt(whatday)===0 || parseInt(whatTimeFragment)===0 ){
            dispatch(setDialogMsg('錯誤','請確認所有選項已填入'))
        }
        else{
            let orderPlace = {
                id:placeId,
                placeName,
                onDay:whatday,
                timeFragment:whatTimeFragment,
                placeLat,
                placeLon
            }
            
            //
            orderPlaces.push(orderPlace)
            
            //sort 依照天>時區ID
            orderPlaces.sort(function(placeA,placeB){
                console.log(placeA,placeB)
                let placeAOnDay = parseInt(placeA.onDay)*10,
                placeBOnDay = parseInt(placeB.onDay)*10,
                placeATF = parseInt(placeA.timeFragment),
                placeBTF = parseInt(placeB.timeFragment)
                return (placeAOnDay+placeATF) - (placeBOnDay+placeBTF)
            });

            console.log(orderPlaces)

            dispatch(pushOrderLocationToStore(orderPlaces));

            this.setState({
                whatday:"0",
                whatTimeFragment:"0",
                placeName:"",
                orderPlaces
            })
        }
    }

    autocompleteForm(options=[]){
        let lis = []
        options.map((option,index)=>{            
            lis.push(
                <li 
                    key={index} 
                    value={option.id}   
                    onClick={()=>{
                        this.setState({
                            placeName:option.name,
                            placeId:option.id,
                            showAutocomplete:false,
                            selectFromAutocomplete:true,
                            placeLat:option.lat,
                            placeLon:option.lon,
                            autocompleteErr:''
                        })                        
                    }}                 
                >
                    {option.name}
                </li>
            )
        })
        return(
            <ul id="autocomplete">
                {lis.length===0?
                    <li className="ul-help text-center">
                        <small><a href="#">沒有結果嗎?立即幫助我們新增</a></small>
                    </li>
                    :
                    lis
                }
            </ul>
        )
    }

    render() {
        let { autocompleteForm } = this;
        let { showAutocomplete,placeName,selectFromAutocomplete,orderPlaces } = this.state
        let { askTravelInfoReducer } = this.props,
        {autocompleteDatas,inputBundle={}} = askTravelInfoReducer;
        let {days:travelDays=0} = inputBundle
        travelDays = parseInt(travelDays)
        console.log(autocompleteDatas,selectFromAutocomplete)
        return (
            <div id="askAuto">                
                <br />
                <div className="row">
                    <div className="col">
                    {/* order form for mobile */}
                    <div className="d-md-none w-100 text-center">
                        <h3>指定地點</h3>
                        <br/>
                        {this.orderForm()}
                        <hr/>
                    </div>
                    <div className="w-100 text-center">
                        <h3>有想指定的地點嗎?</h3>
                        <small className="text-secondary">※如果沒有特別指定也可以直接開始</small>
                    </div>
                    <br/>
                        <div className="form-group">
                            <div className="form-row">
                                <div className="col-md-3 col-12">
                                    <label>地點名稱?</label>
                                </div>
                                <div className="col-md-6 col-12">
                                    <input 
                                        type="text" 
                                        className="form-control"
                                        name='placeName'
                                        value={placeName}
                                        onChange={(e)=>{
                                            let { dispatch } = this.props
                                            dispatch(pollingPlaces(e.target.value))
                                            this.setState({
                                                [e.target.name]:e.target.value,                                                
                                                showAutocomplete:true
                                            })
                                        }}
                                        onBlur={()=>{                                           
                                            setTimeout(()=>{
                                                if(!this.state.selectFromAutocomplete){
                                                    this.setState({
                                                        placeName:'',
                                                        autocompleteErr:'請點擊搜尋建議選項'
                                                    })
                                                }
                                                this.setState({
                                                    showAutocomplete:false,
                                                    selectFromAutocomplete:false
                                                })                                                
                                            },200)
                                        }}
                                    />
                                    {placeName==='' || !showAutocomplete?null:autocompleteForm(autocompleteDatas)}
                                    <small className="text-danger">{this.state.autocompleteErr}</small>
                                </div>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="form-row">
                                <div className="col-md-3 col-6">
                                    <label>想在第幾天去?</label>
                                </div>
                                <div className="col-md-3 col-6">
                                    <select 
                                        className="form-control" 
                                        name="whatday"
                                        value={this.state.whatday}
                                        onChange={(e)=>{                                            
                                            this.handleInputOnChange(e)
                                        }}
                                    >
                                        <option disabled selected value="0">請選擇</option>
                                        {this.onDaySelectOption(travelDays)}                                        
                                    </select>
                                </div>
                                <br/>
                                <br/>
                                <div className="col-md-3 col-6">
                                    <label>想在哪個時段去?</label>
                                </div>
                                <div className="col-md-3 col-6">
                                    <select 
                                        className="form-control" 
                                        name=""
                                        id=""
                                        name="whatTimeFragment"
                                        value={this.state.whatTimeFragment}
                                        onChange={(e)=>{                                            
                                            this.handleInputOnChange(e)
                                        }}
                                    >
                                        <option disabled selected value="0">請選擇</option>
                                        <option value="1">早上</option>
                                        <option value="2">下午</option>
                                        <option value="3">晚上</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <button 
                            className="btn text-white bg-secondary w-100"
                            onClick={()=>{
                                this.addPlace()
                            }}
                        >
                            新增地點
                        </button>
                        <hr />
                        <br/>
                        <div className="w-100 text-center"><h3>準備好出發了嗎?</h3></div>
                        <br/>
                        <div  
                            className="start-link"
                            onClick={()=>{
                                this.startAutoSchedule(orderPlaces.length===0?1:2)
                            }}
                        >
                            <div
                                className="card text-white bg-success mb-3"
                            >
                                <div className="card-body text-center">
                                    <h4 className="card-title">開始全自動排程</h4>
                                    <p className="card-text">敬請稍待讓我們為您安排旅遊</p>
                                </div>                                
                            </div>                            
                        </div>
                    </div>
                    <div className="col-md-6 col-12 d-none d-md-flex">
                        {this.orderForm()}
                    </div>
                </div>
            </div>
        );
    }
}

let mapStateToProps = (state)=>{
    return{
        askTravelInfoReducer:state.AskTravelInfoReducer        
    }
}

AskAuto.propTypes={
    dispatch:PropTypes.func,
    push:PropTypes.func,
    history:PropTypes.object,
    askTravelInfoReducer:PropTypes.object
}

export default withRouter(connect(mapStateToProps)(AskAuto));