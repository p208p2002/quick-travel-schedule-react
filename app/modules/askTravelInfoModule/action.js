import axios from 'axios';
import { API_HOST } from 'App/env.js'
import { setAppLoadingState,setDialogMsg } from 'AppModules/mainModules/action'
export const ASKTRAVELINFO_DISPATCH_FORM_INPUT = 'ASKTRAVELINFO_DISPATCH_FORM_INPUT';
export const ASKTRAVELINFO_GET_AVAILABLE_AREA = 'ASKTRAVELINFO_GET_AVAILABLE_AREA';
export const ASKTRAVELINFO_ORDER_LOCATION = 'ASKTRAVELINFO_ORDER_LOCATION';
// export const ASKTRAVELINFO_DISPATCH_FROM2_INPUT = 'ASKTRAVELINFO_DISPATCH_FROM2_INPUT';
export const ASKTRAVELINFO_REPLACE_REDUCER = 'ASKTRAVELINFO_REPLACE_REDUCER';
export const ASKTRAVELINFO_PLACES_POLLING = 'ASKTRAVELINFO_PLACES_POLLING';


// export function pushForm2InputToStore(customizeList){
//     return{
//         type:ASKTRAVELINFO_DISPATCH_FROM2_INPUT,
//         customizeList:customizeList
//     }
// }

export function pollingPlaces(searchKey){
    return (dispatch)=>{
        axios.post(API_HOST+'/polling/places',{
            token:sessionStorage.getItem('user'),
            placeName:searchKey
        })
        .then((res)=>{
            dispatch({
                type:ASKTRAVELINFO_PLACES_POLLING,
                autocompleteDatas:res.data
            })
        })
    }
}

export function pushOrderLocationToStore(orderList){
    return{
        type:ASKTRAVELINFO_ORDER_LOCATION,
        orderList:orderList
    }
}

export function getAvaliableArea(){    
    return (dispatch)=>{
        dispatch(setAppLoadingState(true))
        axios.post(API_HOST + '/get-available-area',{
            token:sessionStorage.getItem('user')
        })
            .then(function(response){
                // console.log(response.data.area,fakeAvailableArea.data)
                dispatch({
                    type:ASKTRAVELINFO_GET_AVAILABLE_AREA,
                    area:response.data.area
                });                
                dispatch(setAppLoadingState(false))
            })
            .catch(function(error){
                console.error(error.response.data.msg);
                dispatch(setDialogMsg('錯誤',error.response.data.msg))                
                dispatch(setAppLoadingState(false))                
            })        
    }
}

export function pushFormInputToStore(bundle){    

    return({
        type:ASKTRAVELINFO_DISPATCH_FORM_INPUT,
        inputBundle:bundle
    })
}

export function replaceReducer(reducer){
    return{
        type:ASKTRAVELINFO_REPLACE_REDUCER,
        reducer
    }
}