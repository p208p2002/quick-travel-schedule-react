import React, { Component } from 'react';
import PropTypes from 'prop-types';
class View extends Component {    

    constructor(props){
        super(props)
        this.state={
            defaultDay:props.defaultDay
        }        
        this.selectOnchange = this.selectOnchange.bind(this);        
    }

    componentWillReceiveProps(props){        
        this.setState({defaultDay: props.defaultDay});
    }

    selectOnchange(event){        
        this.setState({defaultDay: event.target.value});
    }

    render() {
        let Options = [];
        for (let i = 0; i < (this.props.optionNum - 1); i++) {
            Options.push(
                <option key={i} value={i + 1}>第{i + 1}天</option>
            )
        }
        return (
            <div className="input-group width-20 onDay-ab">
                <select 
                    id="oredrHotelSelect" 
                    className="form-control"
                    value={this.state.defaultDay}
                    onChange={this.selectOnchange}
                >
                    {Options}
                </select>
            </div>
        )
    }
}

View.propTypes={
    optionNum:PropTypes.number,
    defaultDay:PropTypes.string
}

export default View;