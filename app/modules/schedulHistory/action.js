import axios from 'axios';
import { API_HOST } from 'App/env.js'
import { setAppLoadingState,setDialogMsg } from 'AppModules/mainModules/action.js'
import { replaceReducer as travelCardReplace } from 'App/modules/travelCardModule/action.js';
import { replaceReducer as askTravelInfoReplace } from 'App/modules/askTravelInfoModule/action.js';

export const SCHEDULE_HISTORY_FETCH_RECORD_SUCCESS = 'SCHEDULE_HISTORY_FETCH_RECORD_SUCCESS';
export const SCHEDULE_HISTORY_FETCH_RECORD_FAIL = 'SCHEDULE_HISTORY_FETCH_RECORD_FAIL';

export function fetchRecord(sid){
    return (dispatch)=>{
        dispatch(setAppLoadingState(true));
        axios.get(API_HOST+'/travel-schedule-record?sid='+sid)
        .then(function(res){
            // console.log(res)
            let reducers = JSON.parse(res.data.bundle.reducers);            
            let {askTravelInfoReducer,travelCardModeReducer} = reducers
            // console.log(askTravelInfoReducer,travelCardModeReducer);
            
            dispatch(askTravelInfoReplace(askTravelInfoReducer));
            dispatch(travelCardReplace(travelCardModeReducer));
            dispatch(setAppLoadingState(false));
            dispatch({type:SCHEDULE_HISTORY_FETCH_RECORD_SUCCESS})
        })
        .catch(function(err){
            console.error(err);
            let {response} = err,
            {data} = response,
            {msg="ERROR"} = data;
            dispatch(setDialogMsg('發生錯誤',msg));
            dispatch({type:SCHEDULE_HISTORY_FETCH_RECORD_FAIL})
            dispatch(setAppLoadingState(false));
        })
    }
}