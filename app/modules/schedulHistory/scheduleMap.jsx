import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { singleTimeJudgeId } from 'AppCore/timeTools.js'
// import axios from 'axios';
// import { API_HOST } from 'App/env.js'
//
const { compose, withProps, lifecycle } = require("recompose");
const {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    DirectionsRenderer,
} = require("react-google-maps");

class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {            
            locations:null,
            timeFragment:0,
            fragmentstartAt:[],
            gmapWayPointPlaceIds:null,
            clickAt:1
        }
        this.setShowFragment = this.setShowFragment.bind(this)

        //篩選出指定天數
        let { travelCardModeReducer } = props
        let { selectPlaceTuples } = travelCardModeReducer
        let orderDay = parseInt(props.onDay)

        this.selectPlaceTuples = selectPlaceTuples.filter((data)=>{
            let {day} = data
            day = parseInt(day)
            if(orderDay === day){
                return true
            }                
            return false
        })
    }

    componentWillMount(){
        this.setState({
           locations:this.selectPlaceTuples
        });
        //
        let locations = this.selectPlaceTuples;
        let filterLocationFragment= (ary,fId)=>{
            let newAry = Object.assign([],ary);
            newAry = newAry.filter((location)=>{
                if(parseInt(location.timeFragmentId) === fId){
                    return true
                }
                return false
            })
            return newAry
        }
        let morningLocations = filterLocationFragment(locations,1)
        let afternoonLocations = filterLocationFragment(locations,2)
        let { fragmentstartAt } = this.state
        fragmentstartAt.push(
            morningLocations[morningLocations.length-1],
            afternoonLocations[afternoonLocations.length-1]
        );
        this.setState({
            fragmentstartAt
        })
    }

    componentDidMount(){
        //        
        let { askTravelInfoReducer={},onDay } = this.props,
        { inputBundle={} } = askTravelInfoReducer,
        {startAtTimeId=0} = inputBundle;
                    
        onDay = parseInt(onDay);
        if(onDay === 1){
            this.setShowFragment(startAtTimeId);
            this.setState({
                clickAt:startAtTimeId
            })
        }
        else{
            this.setShowFragment(1);
            this.setState({
                clickAt:1
            })
        }        


        console.log('mount',this.props);
    }

    componentDidUpdate(){
        console.log('update',this.props.travelCardModeReducer);
    }

    setShowFragment(id) {
        let {selectPlaceTuples:locations={}} = this;
        locations=locations.filter((location)=>{            
            if(parseInt(id) === 0){
                return true
            }
            else if(parseInt(location.timeFragmentId) === parseInt(id)){
                return true;
            }
            return false;
        });
        this.setState({
            locations,
            timeFragment:id
        })
    }

    render() {        
        let {askTravelInfoReducer} =this.props
        let { clickAt } = this.state
        let waypointsAry = []
        this.state.locations.map((data)=>{
            let {lat,lon} = data            
            waypointsAry.push({
                location: new google.maps.LatLng(String(lat),String(lon)),                
                stopover: true
            })
        })
        //如果是第一天的開始時段，將開始旅行點加入規劃
        let travelStartTime = askTravelInfoReducer.inputBundle.startAtTime;
        let travelFragmentId = singleTimeJudgeId(travelStartTime);        
        if(parseInt(this.props.onDay)===1 && this.state.timeFragment===travelFragmentId){            
            waypointsAry.unshift({location:askTravelInfoReducer.inputBundle.startAt});
        }//否則加入上一時段最後一個點作為起點
        else if(this.state.timeFragment === 2){            
            let { lon,lat } = this.state.fragmentstartAt[0]            
            waypointsAry.unshift({location:new google.maps.LatLng(String(lat),String(lon))})
        }
        else if(this.state.timeFragment === 3){
            let { lon,lat } = this.state.fragmentstartAt[1]
            waypointsAry.unshift({location:new google.maps.LatLng(String(lat),String(lon))})
        }

        //如果是最後一天的結束時段，將結束地點點加入規劃
        let travelEndTime = askTravelInfoReducer.inputBundle.endAtTime;
        let endTravelFragmentId = singleTimeJudgeId(travelEndTime);
        let travelDays = askTravelInfoReducer.inputBundle.days;        
        if(parseInt(this.props.onDay)===parseInt(travelDays) && this.state.timeFragment===endTravelFragmentId){            
            waypointsAry.push({location:askTravelInfoReducer.inputBundle.endAt});
        }
        
        let waypointsAryLen = waypointsAry.length;
        let startAt,endAt = null;
        if(waypointsAryLen !==0){
            //取出開始與結束
            startAt = waypointsAry[0].location;
            endAt = waypointsAry[waypointsAryLen-1].location; 
            //剔除開始與結束
            waypointsAry = waypointsAry.slice(1,waypointsAryLen-1);        
        }
        const MapWithADirectionsRenderer = compose(
            withProps({
                googleMapURL: "https://maps.googleapis.com.tw/maps/api/js?key=AIzaSyAidB8XIVQpo0G6c4kuQ31wOInYlkMuovM&v=3.exp&libraries=geometry,drawing,places",
                loadingElement: <div style={{ height: `100%` }} />,
                containerElement: <div style={{ height: `400px` }} />,
                mapElement: <div style={{ height: `100%` }} />,
            }),
            withScriptjs,
            withGoogleMap,
            lifecycle({
                componentDidMount() {
                    const DirectionsService = new google.maps.DirectionsService();

                    DirectionsService.route({
                        origin: startAt,
                        destination: endAt,
                        waypoints: waypointsAry,
                        optimizeWaypoints: true,
                        travelMode: 'DRIVING'
                    }, (result, status) => {
                        if (status === google.maps.DirectionsStatus.OK) {
                            this.setState({
                                directions: result,                                
                            });                                                                                     
                            console.log(result.geocoded_waypoints)
                            // let getPlaceName = (placeId)=>{                                
                            //     axios.get(API_HOST+'/request-google-map-place/'+placeId)
                            //         .then((res)=>{
                            //             console.log(res.data.result.name)                                        
                            //         })
                            //         .catch((err)=>{
                            //             console.error(err)
                            //         })
                            // }
                            // getPlaceName('EjQ0MDflj7DngaPlj7DkuK3luILopb_lsa_ljYDpgKLnlLLot68yMOW3tzI45byEMS056Jmf')
                            // let route = result.routes[0]
                            // for (var i = 0; i < route.legs.length; i++) {
                            //     var routeSegment = i + 1;
                            //     console.log(routeSegment,route.legs[i].start_address,route.legs[i].end_address,route.legs[i].distance.text)                                
                            // }                       
                        } else {
                            console.error(`error fetching directions ${result}`);
                        }
                    });
                }
            })
        )(props =>
            <div>
                <GoogleMap
                    defaultZoom={7}
                    defaultCenter={new google.maps.LatLng(23.903687,121.079370)}
                >
                    {props.directions && <DirectionsRenderer directions={props.directions} />}
                </GoogleMap>                              
            </div>
        );
       
        return (
            <div>
                <span>請選擇顯示路徑</span><br/>                
                <button 
                    tpye="button" 
                    onClick={() => { 
                        this.setShowFragment(1) 
                        this.setState({
                            clickAt:1
                        })
                    }}
                    className={'btn btn-sm ' + (clickAt === 1?'btn-info-c':'') }
                >
                    早上
                </button>
                &nbsp;
                <button 
                    tpye="button" 
                    onClick={() => { 
                        this.setShowFragment(2) 
                        this.setState({
                            clickAt:2
                        })
                    }}
                    className={'btn btn-sm ' + (clickAt === 2?'btn-info-c':'') }
                >
                    下午
                </button>
                &nbsp;
                <button 
                    tpye="button" 
                    onClick={() => { 
                        this.setShowFragment(3) 
                        this.setState({
                            clickAt:3
                        })
                    }}
                    className={'btn btn-sm ' + (clickAt === 3?'btn-info-c':'') }
                >
                    晚上
                </button>
                <br/><br/>
                <MapWithADirectionsRenderer />                
            </div>
        );
    }
}

Map.propTypes={
    askTravelInfoReducer:PropTypes.object,
    travelCardModeReducer:PropTypes.object,
    onDay:PropTypes.number
}

let mapStateToProps = (state)=>{
    return{
        askTravelInfoReducer:state.AskTravelInfoReducer,
        travelCardModeReducer:state.TravelCardModeReducer,        
    }
}

export default connect(mapStateToProps)(Map);