import {
    SCHEDULE_HISTORY_FETCH_RECORD_FAIL,
    SCHEDULE_HISTORY_FETCH_RECORD_SUCCESS
} from './action.js';

const initstate = ()=>{
    return{
        loading:true
    }
}

export default (state = initstate(),action)=>{ 
    switch (action.type){
        case SCHEDULE_HISTORY_FETCH_RECORD_SUCCESS:            
            return Object.assign({},state,{
                loading:false
            })
        case SCHEDULE_HISTORY_FETCH_RECORD_FAIL:
        default:
            return state
    }
}