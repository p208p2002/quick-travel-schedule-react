import React, { Component } from 'react';
import {connect} from 'react-redux'
import PropTypes from 'prop-types';

class Index extends Component {
    constructor(props){
        super(props)        
    }


    componentDidMount(){
        // console.log(this.props.travelInfoState.inputBundle.days);
    }

    showSelcetPlace(timeFragmentId,nowDay){
        let {selectPlaceTuples=[]} = this.props.travelCardModeReducer
        let filterResult = selectPlaceTuples.filter((tuple)=>{
            if(parseInt(tuple.timeFragmentId) === parseInt(timeFragmentId)){
                return true;
            }
            return false;
        })
        let output = ''

        //篩選 只顯示目前規劃
        filterResult = 
            filterResult.filter((data)=>{
                if(data.day === nowDay){
                    return true;
                }
                return false;
            });

        //展開
        filterResult.map((data)=>{
            output = output+data.placeName + ','
        })
        return(
            <span>{output}</span>
        )
    }

    render() {
        let List =[];        
        let days = parseInt(this.props.travelInfoState.inputBundle.days);        
        for(let i=0;i<days;i++){
            List.push(
                <div key={i}>
                    <h4><b>第{i+1}天</b></h4>
                    <span><b>早上:</b>{ this.showSelcetPlace(1,i+1) }</span><br/>
                    <span><b>中午:</b>{ this.showSelcetPlace(2,i+1) }</span><br/>
                    <span><b>晚上:</b>{ this.showSelcetPlace(3,i+1) }</span><br/>
                </div>
            )
        }
        return (
            <div>
                {List}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        travelInfoState: state.AskTravelInfoReducer,
        travelCardModeReducer: state.TravelCardModeReducer
    }
}

Index.propTypes={
    travelInfoState:PropTypes.object,
    travelCardModeReducer:PropTypes.object
}
 
export default connect(mapStateToProps)(Index);