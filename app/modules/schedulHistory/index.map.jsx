import React, { Component } from 'react';
import {connect} from 'react-redux'
import PropTypes from 'prop-types';
import ScheduleMap from './scheduleMap.jsx';
import { withRouter } from "react-router-dom";
import { fetchRecord } from './action.js';
import { initAutoSchedule } from 'App/modules/autoScheduleModule/action.js'
import { initTravelCard } from 'App/modules/travelCardModule/action.js'

class Index extends Component {
    constructor(props){
        super(props)
        this.state={
            loading:true
        }
    }

    componentWillMount(){
        setTimeout(()=>{
            this.setState({
                loading:false
            })
        },3000)
    }

    componentWillReceiveProps(nextProps){
        let {loading} = nextProps.scheduleHistoryReducer;
        if(loading === false && this.state.loading === true){
            setTimeout(()=>{
                this.setState({
                    loading:false
                })
            },0)
        }
    }

    componentWillMount(){
        // console.log('mount',this.props);
        console.log(this.props.travelCardModeReducer)
        const search = this.props.location.search; 
        const params = new URLSearchParams(search);
        const scheduleId = params.get('sid'); // bar
        let { dispatch } = this.props;
        dispatch(fetchRecord(scheduleId));
        console.log('sid',scheduleId);
    }

    componentWillUnmount(){
        let {dispatch} = this.props
        dispatch(initAutoSchedule)
        dispatch(initTravelCard)
    }

    showSelcetPlace(timeFragmentId,nowDay){
        let { travelInfoState={} } = this.props,
        { inputBundle={} } = travelInfoState,
        {endAtTimeId=0,startAtTimeId=0,days:totalTravelDays=0,
            startAt,endAt
        } = inputBundle;

        // 
        let {selectPlaceTuples=[]} = this.props.travelCardModeReducer
        let filterResult = selectPlaceTuples.filter((tuple)=>{
            if(parseInt(tuple.timeFragmentId) === parseInt(timeFragmentId)){
                return true;
            }
            return false;
        })
        // let output = ''

        //篩選 只顯示目前規劃
        filterResult = 
            filterResult.filter((data)=>{
                if(data.day === nowDay){
                    return true;
                }
                return false;
            });

        //展開
        let output = []
        let indexAlphabet = 66;
        
        //如果與開始天數&時段匹配放入開始點
        timeFragmentId = parseInt(timeFragmentId)
        startAtTimeId = parseInt(startAtTimeId)
        totalTravelDays = parseInt(totalTravelDays)
        if(nowDay===1 && (timeFragmentId === startAtTimeId)){
            indexAlphabet = 65;
            output.push(<div>&nbsp;{ String.fromCharCode(indexAlphabet) }&nbsp;-&nbsp;<b>{startAt}</b><br/></div>)
            indexAlphabet++
        }

        //放入旅程點
        if(nowDay !==1 && timeFragmentId ===1){
            indexAlphabet = 65;
        }
        filterResult.map((data)=>{            
            output.push(<div>&nbsp;{ String.fromCharCode(indexAlphabet) }&nbsp;-&nbsp;{data.placeName}<br/></div>)
            indexAlphabet++
        })

        //如果與開始天數&時段匹配放入結束點
        if(nowDay === totalTravelDays && (timeFragmentId === endAtTimeId)){
            output.push(<div>&nbsp;{ String.fromCharCode(indexAlphabet) }&nbsp;-&nbsp;<b>{endAt}</b><br/></div>)
        }
        
        return(
            <div> {output} </div>
        )
    }

    render() {        
        let { loading=true } = this.state
        if(loading){
            return(
                <div>讀取中</div>
            );
        }
        let List =[];        
        let days = parseInt(this.props.travelInfoState.inputBundle.days);        
        for(let i=0;i<days;i++){
            List.push(
                <div key={i}>

                    {/* pc */}
                    <div className="row">
                        <div className="col-md-4 d-none d-md-block">
                            <h4><b>第{i+1}天</b></h4>
                            <span><b>早上:</b><br/>{ this.showSelcetPlace(1,i+1) }</span><br/>
                            <span><b>下午:</b><br/>{ this.showSelcetPlace(2,i+1) }</span><br/>
                            <span><b>晚上:</b><br/>{ this.showSelcetPlace(3,i+1) }</span><br/>
                        </div>
                        <div className="col-12 col-md-8">
                            <ScheduleMap onDay={parseInt(i+1)} />
                        </div>

                        {/* mobile */}
                        <div className="col-12 d-md-none">
                            <h4><b>第{i+1}天</b></h4>
                            <span><b>早上:</b><br/>{ this.showSelcetPlace(1,i+1) }</span><br/>
                            <span><b>下午:</b><br/>{ this.showSelcetPlace(2,i+1) }</span><br/>
                            <span><b>晚上:</b><br/>{ this.showSelcetPlace(3,i+1) }</span><br/>
                        </div>
                    </div>
                    <hr/>                    
                </div>
            )
        }
        return (
            <div>                
                {List}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        travelInfoState: state.AskTravelInfoReducer,
        travelCardModeReducer: state.TravelCardModeReducer,
        scheduleHistoryReducer: state.ScheduleHistoryReducer
    }
}

Index.propTypes={
    travelInfoState:PropTypes.object,
    travelCardModeReducer:PropTypes.object,
    dispatch:PropTypes.func,
    scheduleHistoryReducer:PropTypes.object,
    location:PropTypes.object
}
 
export default withRouter(connect(mapStateToProps)(Index));