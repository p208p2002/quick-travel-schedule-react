import React, { Component } from 'react';
import checkboxPenOutline from 'AppSrc/icons/checkbox-pen-outline.png';
import eye from 'AppSrc/icons/eye_w.png';
import deleteImg from 'AppSrc/icons/delete_w.png';
import { getRecords,delTravel,editTravelName } from './action.js';
import { connect } from 'react-redux';
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';

import './index.css';

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.delTravel = this.delTravel.bind(this)
    }

    componentDidMount(){
        let { dispatch } = this.props;
        dispatch(getRecords());
    }

    delTravel(id){
        let { dispatch } = this.props;
        dispatch(delTravel(id));
    }

    render() {
        let { myTravelReducer,dispatch } = this.props,
        {records} = myTravelReducer;
        console.log(records);
        return (
            <div id="myTravel">
                <br/>
                <div className="row">
                    <div className="col">名稱</div>
                    <div className="col">操作</div>
                    <div className="col d-none d-sm-flex">建立日期</div>                    
                </div>
                <hr />
               {records.map((record,index)=>{                   
                   return(
                        <div key={index} className="row">
                            <div className="col">
                                {this.state['travelName'+index] === 'edit'?
                                <input
                                    id={'travelNameInput'+index}
                                    type="text" 
                                    defaultValue={record.travelName}
                                    onKeyPress={(e) => {
                                        if (e.key === "Enter") {
                                            this.setState({
                                                ['travelName'+index]:'text'
                                            })                                        
                                            if(e.target.value !== record.travelName){
                                                dispatch(editTravelName(record.id,e.target.value))
                                            }
                                        }
                                      }}
                                    onBlur={(e)=>{
                                        this.setState({
                                            ['travelName'+index]:'text'
                                        })                                        
                                        if(e.target.value !== record.travelName){
                                            dispatch(editTravelName(record.id,e.target.value))
                                        }
                                    }}
                                />
                                :
                                <span
                                    className="editName"
                                    onClick={()=>{                            
                                        this.setState({
                                            ['travelName'+index]:this.state['travelName'+index] === 'edit'?'text':'edit'
                                        })
                                        setTimeout(()=>{
                                            let node = document.getElementById('travelNameInput'+index)
                                            node.focus()
                                            node.setSelectionRange(0,node.value.length)                                            
                                        },0)                          
                                    }}
                                >
                                    {record.travelName}
                                </span>
                                }&nbsp;
                                <img 
                                    src={checkboxPenOutline} 
                                    width="12" 
                                    alt="編輯名稱"
                                    className="editName"
                                    name={'travelName'+index}
                                    onClick={()=>{                            
                                        this.setState({
                                            ['travelName'+index]:this.state['travelName'+index] === 'edit'?'text':'edit'
                                        })
                                        setTimeout(()=>{
                                            let node = document.getElementById('travelNameInput'+index)
                                            node.focus()
                                            node.setSelectionRange(0,node.value.length)                                            
                                        },0)                          
                                    }}
                                />
                            </div>                    
                            <div className="col">
                                <Link className="btn btn-success" to={'/schedule-result?sid=' + record.id}>
                                    <img height="24" src={eye} alt="查看旅遊"/>
                                </Link>
                                &nbsp;
                                
                                <button 
                                    className="btn btn-danger"
                                    onClick={()=>{this.delTravel(record.id)}}
                                >
                                    <img height="24" src={deleteImg} alt="刪除旅遊"/>
                                </button>
                            </div>
                            <div className="col-12 col-sm date-m">{record.createAt}</div>                            
                            <br/>
                            <br/>
                    </div>
                   )
               })}
            </div>        
        );
    }
}

Index.propTypes={
    dispatch:PropTypes.func,
    myTravelReducer:PropTypes.object
}

let mapStateToProps = (state)=>{
    return{
        myTravelReducer:state.MyTravelReducer
    }
}

export default connect(mapStateToProps)(Index);