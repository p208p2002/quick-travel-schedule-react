import {
    MY_TRAVEL_GET_RECORDS_FAIL,
    MY_TRAVEL_GET_RECORDS_SUCCESS
} from './action'

const initState = ()=>{
    return{
        records:[]
    }
}

export default (state=initState(),action)=>{
    switch (action.type) {
        case MY_TRAVEL_GET_RECORDS_SUCCESS:
            return Object.assign({},state,{
                records:action.records
            })            

        case MY_TRAVEL_GET_RECORDS_FAIL:
        default:
            return state;
    }
}