import axios from 'axios';
import { API_HOST } from 'App/env.js'
import { setAppLoadingState,setDialogMsg } from 'AppModules/mainModules/action.js'

export const MY_TRAVEL_GET_RECORDS_SUCCESS = 'MY_TRAVEL_GET_RECORDS_SUCCESS';
export const MY_TRAVEL_GET_RECORDS_FAIL = 'MY_TRAVEL_GET_RECORDS_FAIL';

export const MY_TRAVEL_DEL_TRAVEL_SUCCESS = 'MY_TRAVEL_DEL_TRAVEL_SUCCESS';
export const MY_TRAVEL_DEL_TRAVEL_FAIL = 'MY_TRAVEL_DEL_TRAVEL_FAIL';

export const MY_TRAVEL_UPDATE_TRAVEL_NAME = 'MY_TRAVEL_UPDATE_TRAVEL_NAME';

export function getRecords(){
    return (dispatch)=>{
        dispatch(setAppLoadingState(true));
        axios.post(API_HOST+'/my-travel/get-records',{
            token:sessionStorage.getItem('user')
        })
        .then(function(res){
            console.log(res)
            dispatch({
                type:MY_TRAVEL_GET_RECORDS_SUCCESS,
                records:res.data
            });
            dispatch(setAppLoadingState(false));
        })
        .catch(function(err){
            console.log(err)
            dispatch({
                type:MY_TRAVEL_GET_RECORDS_FAIL
            })
            dispatch(setDialogMsg('錯誤','獲取記錄失敗'));
            dispatch(setAppLoadingState(false));
        })
    }
}

export function delTravel(id){
    return (dispatch)=>{
        dispatch(setAppLoadingState(true))
        axios.post(API_HOST+'/my-travel/del-record',{
            token:sessionStorage.getItem('user'),
            id
        })
        .then(()=>{            
            dispatch(getRecords())
        })
        .catch(()=>{
            dispatch(setDialogMsg('錯誤','獲取記錄失敗'));
            dispatch(setAppLoadingState(false));
        })
    }
}

export function editTravelName(tid,travelName){
    return (dispatch)=>{
        dispatch({type:MY_TRAVEL_UPDATE_TRAVEL_NAME})
        dispatch(setAppLoadingState(true))
        axios.post(API_HOST+'/my-travel/edit-name',{
            token:sessionStorage.getItem('user'),
            tid,
            travelName
        })
        .then(()=>{
            dispatch(getRecords())
        })
        .catch(()=>{
            dispatch(setAppLoadingState(false))
        })
    }
}