export const BASENAME = '' // also set basename at webpack.config for file-loader
export const TRAVELER_HOST = 'http://localhost/quick-travel-schedule-laravel/public';
export const API_HOST = 'http://localhost/quick-travel-schedule-laravel/public/api';
export const IMG_SERVER_HOST = 'http://localhost/quick-travel-schedule-laravel/public';

// export const BASENAME = '/qts-react/public'
// export const TRAVELER_HOST = 'https://traveler.thecodingday.com';
// export const API_HOST = 'https://traveler.thecodingday.com/api';
// export const IMG_SERVER_HOST = 'https://traveler.thecodingday.com/';