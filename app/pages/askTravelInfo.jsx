import React, { Component } from 'react';
import AskInfo from 'AppModules/askTravelInfoModule/main.jsx'
// import Footer from 'AppModules/mainModules/footer/index.jsx'
import Nav from 'AppModules/mainModules/topnav/main.jsx'



class AskTravelInfoView extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div>                
                <Nav/>                
                <div className="container">
                    <AskInfo/>
                </div>                
            </div>
         )
    }
}
 
export default AskTravelInfoView;