import React, { Component } from 'react';
import Nav from 'AppModules/mainModules/topnav/main.jsx'
import SchedulHistory from 'AppModules/schedulHistory/index.map.jsx'
import {Helmet} from "react-helmet";
import logo from 'AppSrc/img/logo.png';
import { 
    FacebookShareButton,FacebookIcon,
    EmailShareButton,EmailIcon,
    GooglePlusShareButton,GooglePlusIcon,
    TwitterShareButton,TwitterIcon
 } from 'react-share'

class View extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
        <div>
            <Nav/>
            <Helmet>
                <title>QTS - 快速旅遊規劃</title>                
                <meta property="og:url"           content={location.href} />
                <meta property="og:type"          content="website" />
                <meta property="og:title"         content="QTS - 快速旅遊規劃" />
                <meta property="og:description"   content="來一場說走就走的旅行" />
                <meta property="og:image"         content={logo} />
            </Helmet>

            <br/>            
            <div className="container">
                <div className="text-center">
                    <h3>感謝您使用QTS規劃旅程</h3>
                    <div className="Demo__some-network">
                        <FacebookShareButton
                            url={location.href}
                            quote={'我在QTS規劃了旅遊，與我一同出發吧!'}
                            className="Demo__some-network__share-button">
                            <FacebookIcon
                            size={32}
                            round />
                        </FacebookShareButton>                    
                    </div>

                    <div className="Demo__some-network">
                        <GooglePlusShareButton
                            url={location.href}
                            className="Demo__some-network__share-button">
                            <GooglePlusIcon
                            size={32}
                            round />
                        </GooglePlusShareButton>                    
                    </div>

                    <div className="Demo__some-network">
                        <TwitterShareButton
                            url={location.href}
                            title={'我在QTS規劃了旅遊，與我一同出發吧!'}
                            className="Demo__some-network__share-button">
                            <TwitterIcon
                            size={32}
                            round />
                        </TwitterShareButton>                       
                    </div>

                    <div className="Demo__some-network">
                        <EmailShareButton                            
                            subject={'我在QTS規劃了旅遊，與我一同出發吧!'}
                            body={location.href}
                            className="Demo__some-network__share-button">
                            <EmailIcon
                            size={32}
                            round />
                        </EmailShareButton>
                    </div>
                </div>
                <br/>
                <SchedulHistory/>
            </div>
        </div> 
    );
    }
}
 
export default View;