import React, { Component } from 'react';
const { compose, withProps, lifecycle } = require("recompose");
const {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  DirectionsRenderer,
} = require("react-google-maps");



class Index extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {  }
    // }
    render() { 

        const MapWithADirectionsRenderer = compose(
            withProps({
              googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAidB8XIVQpo0G6c4kuQ31wOInYlkMuovM&v=3.exp&libraries=geometry,drawing,places",
              loadingElement: <div style={{ height: `100%` }} />,
              containerElement: <div style={{ height: `400px` }} />,
              mapElement: <div style={{ height: `100%` }} />,
            }),
            withScriptjs,
            withGoogleMap,
            lifecycle({
              componentDidMount() {
                const DirectionsService = new google.maps.DirectionsService();
          
                DirectionsService.route({
                  origin: new google.maps.LatLng(41.8507300, -87.6512600),
                  destination: new google.maps.LatLng(41.8525800, -87.6514100),
                  travelMode: google.maps.TravelMode.DRIVING,
                }, (result, status) => {
                  if (status === google.maps.DirectionsStatus.OK) {
                    this.setState({
                      directions: result,
                    });
                  } else {
                    console.error(`error fetching directions ${result}`);
                  }
                });
              }
            })
          )(props =>
            <GoogleMap
              defaultZoom={7}
              defaultCenter={new google.maps.LatLng(41.8507300, -87.6512600)}
            >
              {props.directions && <DirectionsRenderer directions={props.directions} />}
            </GoogleMap>
          );
        
        return ( 
            <div>
                <h1>test page</h1>
                <MapWithADirectionsRenderer />
                <div className="progress">
                    <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style={{'width':'25%'}}/>
                </div>
            </div>
        )
    }
}
 
export default Index;