import React, { Component } from 'react';
import Nav from 'AppModules/mainModules/topnav/main.jsx';
import MyTravelModule from 'AppModules/myTravelModule/index.jsx';

class MyTravel extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (
            <div>
                <Nav/>
                <div className="container">
                    <MyTravelModule/>
                </div>
            </div>
        );
    }
}
 
export default MyTravel;