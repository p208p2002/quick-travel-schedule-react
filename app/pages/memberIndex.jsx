import React, { Component } from 'react';
import Nav from 'AppModules/mainModules/topnav/main.jsx';
import './memberIndex.css';
import { Link } from 'react-router-dom';


class MemberIndex extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }

    render() { 
        return (
            <div>
                <Nav/>
                <div id="memberIndex" className="container">
                    <div className="row func-block">
                        <div className="col-sm-12 col-12">
                            <Link to='/ask-travel-info'>
                                <div className="card card-block card-bg-1">                                
                                    <span className="func-text">規劃旅遊</span>
                                </div>
                                <br/>
                            </Link>
                        </div>
                        <div className="col-sm col-12">
                            <Link to ="/my-travel">
                                <div className="card card-block card-bg-2">                            
                                    <span className="func-text">我的旅遊</span>
                                </div>
                                <br/>
                            </Link>
                        </div>
                        <div className="col-sm col-12">
                            <Link to='#'>
                                <div className="card card-block card-bg-3">                            
                                    <span className="func-text">帳號設定</span>
                                </div>
                                <br/>
                            </Link>
                        </div>
                    </div>
                </div>  
            </div>
        );
    }
}
 
export default MemberIndex;