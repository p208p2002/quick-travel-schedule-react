import React, { Component } from 'react';
import SelectMode from 'AppModules/mainModules/main/selectMode.jsx'
import Nav from 'AppModules/mainModules/topnav/main.jsx'
class SelectModeView extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (
            <div>
                <Nav/>
                <div className="container">
                    <SelectMode/>
                </div>
            </div>
        )
    }
}
 
export default SelectModeView;