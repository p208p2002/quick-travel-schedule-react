import React, { Component } from 'react';

import Nav from 'AppModules/mainModules/topnav/main.jsx'

// 
import Welcome from 'AppPages/welcome.jsx'
import AskTravelInfo from 'AppPages/askTravelInfo.jsx'
import SelectMode from 'AppPages/selectMode.jsx'

// import History from 'App/history.js'
// import createBrowserHistory from 'App/history.js';
// const history111 = createBrowserHistory();

import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch
  } from 'react-router-dom'

//   import {
//     withScriptjs,
//     withGoogleMap,
//     GoogleMap,
//     Marker,
//   } from "react-google-maps";
  
//   const MapWithAMarker = withScriptjs(withGoogleMap(props =>
//     <GoogleMap
//       defaultZoom={8}
//       defaultCenter={{ lat: -34.397, lng: 150.644 }}
//     >
//       <Marker
//         position={{ lat: -34.397, lng: 150.644 }}
//       />
//     </GoogleMap>
//   ));

{/* <MapWithAMarker
googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAidB8XIVQpo0G6c4kuQ31wOInYlkMuovM&v=3.exp&libraries=geometry,drawing,places"
loadingElement={<div style={{ height: `100%` }} />}
containerElement={<div style={{ height: `650px` }} />}
mapElement={<div style={{ height: `100%` }} />}
/> */}
                        
  

  
class Index extends Component {
    render() { 
        return ( 
            <div>
                <Nav />
                <div className="container">                  
                        <Router>
                            {/* router path */}
                            <Switch>
                                <Route exact path="/" component={ Welcome } />
                                <Route exact path="/index" component={ Welcome } />
                                <Route exact path="/ask-travel-info" component={ AskTravelInfo } />
                                <Route exact path="/select-mode" component={ SelectMode } />
                                <Route component={Welcome} />
                            </Switch>
                        </Router>
                </div>
            </div>
        )
    }
}
 
export default Index;