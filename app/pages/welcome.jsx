import React, { Component } from 'react';
import Footer from 'AppModules/mainModules/footer/index.jsx'
// import Nav from 'AppModules/mainModules/topnav/main.jsx'
import './welcome.css'
import LoginForm from 'AppModules/userModule/loginForm.jsx';
import RegisterForm from 'AppModules/userModule/registerForm.jsx';
import logo from 'AppSrc/img/logo.png';
import upImg from 'AppSrc/img/dual-top.png';
import downImg from 'AppSrc/img/dual-down.png';
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";

class WelcomeView extends Component {
    constructor(props){
        super(props)
        this.state={
            helpBlock1:false,
            helpBlock2:false,
            helpBlock3:false
        }
    }
    componentDidMount(){
        //開發用
        // let {push}=this.props.history;
        // push('/ask-travel-info')
        
        // if(sessionStorage.getItem('user')){            
        //     push('/member-index');
        // }
    }    

    render() { 
        return ( 
           <div id="welcome" className="div">                                
                <div className="container-fluid text-white text-md bg">
                    {/*  */}
                    <img src={logo} className="logo" alt=""/>
                    <div className="d-flex justify-content-end header">                
                        <div className="p-2 text-hover">About QTS</div>
                        <div className="p-2 text-hover">About us</div>                        
                        <div className="p-2 text-hover2 bg-dark-blue" data-toggle="modal" data-target="#loginModal">Sign in</div>
                    </div>
                    <div className="row">
                        <div className="col-md-7 col-12">
                            <div className="main-text">
                                <h1 className="text-shadow">一場說走就走的旅遊</h1>
                                <br/>
                                <p>不用再做事前功課與旅遊安排</p>
                                <p>現在就讓QTS您解決煩惱</p>
                            </div>
                        </div>
                        <div className="col-md-5 col-12">
                            <div className="start-block">
                                <div className="card bg-alpha">
                                    <div className="card-body">
                                        <div className="card-title text-center text-shadow"><h2><b>歡迎使用QTS</b></h2></div>                                        
                                        <div className="text-center">                                                                        
                                                <button type="button" className="btn btn-success btn-lg" data-toggle="modal" data-target="#loginModal">
                                                    立即開始
                                                </button>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                {/*  */}                                
                                <div className="d-none d-md-block">
                                    <RegisterForm/>
                                </div>
                            </div>
                        </div>
                    </div>                                                            
                </div>                
                {/*  */}
                <div className="second-block">
                        <div className="about-qts">
                            <div className="row">
                            <div className="col-md col-12">
                                
                                {/* mobile block */}
                                <div className="d-md-none">
                                    <div 
                                        className="text-sub-title text-center" 
                                        data-toggle="collapse" 
                                        data-target="#helpBlock1" 
                                        aria-expanded="false" 
                                        aria-controls="collapseExample"                                        
                                        onClick={()=>{                                            
                                            this.setState({
                                                helpBlock1:!this.state.helpBlock1
                                            })
                                        }}
                                        style={{cursor:'pointer'}}
                                    >
                                        QTS是什麼?
                                        <br/>
                                        <img src={!this.state.helpBlock1?downImg:upImg} style={{width:'20px',height:'14px',marginTop:'8px'}}/>
                                    </div>
                                    <hr/>
                                    <div className="collapse" id="helpBlock1">
                                            <p className="text-dark-blue text-default">全稱<b>Quick travel schedule(快速旅遊排程)</b>。我們希望可以提供一個快速好用的旅程規劃APP讓所有人都能立即開展開一趟驚豔之旅程</p>
                                            <p className="text-dark-blue text-default">您已經有心目中想去的地方卻又對安排旅程感到苦手嗎?還是只想盡情享受旅程不管其他的規劃?不管如何請讓我們為您服務!</p>
                                            <p className="text-dark-blue text-default">我們提供了兩種模式(推薦與自動模式)，可以依據您的喜好選擇，這將會是為您量身打造的旅程</p>
                                    </div>
                                </div>

                                {/* pc block */}
                                <div className="d-none d-md-block">
                                <div className="text-sub-title text-center">
                                    QTS是什麼?
                                </div>
                                <hr/>
                                <p className="text-dark-blue text-default">全稱<b>Quick travel schedule(快速旅遊排程)</b>。我們希望可以提供一個快速好用的旅程規劃APP讓所有人都能立即開展開一趟驚豔之旅程</p>
                                <p className="text-dark-blue text-default">您已經有心目中想去的地方卻又對安排旅程感到苦手嗎?還是只想盡情享受旅程不管其他的規劃?不管如何請讓我們為您服務!</p>
                                <p className="text-dark-blue text-default">我們提供了兩種模式(推薦與自動模式)，可以依據您的喜好選擇，這將會是為您量身打造的旅程</p>
                                </div>
                            </div>
                            <div className="col-md col-12">

                                {/* mobile block */}
                                <div className="d-md-none">
                                    <div 
                                        className="text-sub-title text-center" 
                                        data-toggle="collapse" 
                                        data-target="#helpBlock2" 
                                        aria-expanded="false" 
                                        aria-controls="collapseExample"
                                        onClick={()=>{                                            
                                            this.setState({
                                                helpBlock2:!this.state.helpBlock2
                                            })
                                        }}
                                        style={{cursor:'pointer'}}
                                    >
                                        推薦模式
                                        <br/>
                                        <img src={!this.state.helpBlock2?downImg:upImg} style={{width:'20px',height:'14px',marginTop:'8px'}}/>
                                    </div>
                                    <hr/>
                                    <div className="collapse" id="helpBlock2">
                                        <p className="text-dark-blue text-default">推薦模式將會跟據您提供的資訊進行地點的推薦，在這個模式中您可以在系統的推薦下參與地點的規劃</p>
                                        <p className="text-dark-blue text-default">每一次選擇將會由系統提供三個點進行選擇，並且可以即時查看其他用戶對於這些地點的評價</p>
                                        <p className="text-dark-blue text-default">您將可以從這三個地點中挑選出感興趣的地點前往。選擇完成後系統會根據之前的輸入再次提供三個地點供您選擇，直到完成您的旅程規劃</p>                                        
                                    </div>
                                </div>

                                {/* pc block */}
                                <div className="d-none d-md-block">
                                    <div className="text-sub-title text-center">
                                        推薦模式
                                    </div>
                                    <hr/>
                                    <p className="text-dark-blue text-default">推薦模式將會跟據您提供的資訊進行地點的推薦，在這個模式中您可以在系統的推薦下參與地點的規劃</p>
                                    <p className="text-dark-blue text-default">每一次選擇將會由系統提供三個點進行選擇，並且可以即時查看其他用戶對於這些地點的評價</p>
                                    <p className="text-dark-blue text-default">您將可以從這三個地點中挑選出感興趣的地點前往。選擇完成後系統會根據之前的輸入再次提供三個地點供您選擇，直到完成您的旅程規劃</p>
                                </div>
                            </div>
                            <div className="col-md col-12">

                                {/* mobile block */}
                                <div className="d-md-none">
                                    <div 
                                        className="text-sub-title text-center" 
                                        data-toggle="collapse" 
                                        data-target="#helpBlock3" 
                                        aria-expanded="false" 
                                        aria-controls="collapseExample"
                                        onClick={()=>{                                            
                                            this.setState({
                                                helpBlock3:!this.state.helpBlock3
                                            })
                                        }}
                                        style={{cursor:'pointer'}}
                                    >
                                        自動模式
                                        <br/>
                                        <img src={!this.state.helpBlock3?downImg:upImg} style={{width:'20px',height:'14px',marginTop:'8px'}}/>
                                    </div>
                                    <hr/>
                                    <div className="collapse" id="helpBlock3">
                                        <p className="text-dark-blue text-default">想要現在就出發去旅行了嗎?那麼就立即開始自動旅遊規劃吧!</p>
                                        <p className="text-dark-blue text-default">在這個模式下只要提供系統最少量的資訊(例如指定地點，或甚至不需要!)，就可以讓系統全自完成您專屬的旅程規劃</p>
                                        <p className="text-dark-blue text-default">不再煩惱玩什麼、不再煩惱吃什麼，讓QTS替您決定然後來一場說走就走的旅遊吧!</p>
                                    </div>
                                </div>

                                {/* pc block */}
                                <div className="d-none d-md-block">
                                    <div className="text-sub-title text-center">
                                        自動模式
                                    </div>
                                    <hr/>
                                    <p className="text-dark-blue text-default">想要現在就出發去旅行了嗎?那麼就立即開始自動旅遊規劃吧!</p>
                                    <p className="text-dark-blue text-default">在這個模式下只要提供系統最少量的資訊(例如指定地點，或甚至不需要!)，就可以讓系統全自完成您專屬的旅程規劃</p>
                                    <p className="text-dark-blue text-default">不再煩惱玩什麼、不再煩惱吃什麼，讓QTS替您決定然後來一場說走就走的旅遊吧!</p>
                                </div>
                            </div>
                            </div>
                        </div>                        
                </div> 

                {/* footer */}
                <div className="footer">
                    <Footer/>
                </div>

                {/* login form */}
                <LoginForm/>
                
           </div>
         )
    }
}

WelcomeView.propTypes={
    history:PropTypes.object
}
 
export default withRouter((WelcomeView));