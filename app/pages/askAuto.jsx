import React, { Component } from 'react';
import Nav from 'AppModules/mainModules/topnav/main.jsx'
import AskAuto from 'AppModules/askTravelInfoModule/AskAuto'
class View extends Component {
    render() { 
        return (  
            <div>
                <Nav/>
                <div className="container">
                    <AskAuto/>
                </div>
            </div>
        );
    }
}
export default View;
