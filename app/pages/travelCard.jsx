import React, { Component } from 'react';
import Nav from 'AppModules/mainModules/topnav/main.jsx'
import TravelCardModule from 'AppModules/travelCardModule/main'
import SchedulHistory from 'AppModules/schedulHistory/index.text.jsx'
class View extends Component {
    constructor(props){
        super(props)
        this.state = {
            openHistory:false
        }
    }
    render() {
        let {openHistory} = this.state;
        return ( 
            <div>
                <Nav/>
                <div className="container">                    
                    <TravelCardModule/>
                    <hr/>
                    {openHistory?(
                        <div>
                            <button type="button" onClick={()=>{
                                this.setState({
                                    openHistory:false
                                })
                            }}>收起紀錄</button>
                            <SchedulHistory/>
                        </div>
                    ):(
                        <button type="button" onClick={()=>{
                            this.setState({
                                openHistory:true
                            })
                        }}>展開紀錄</button>
                    )}
                </div>
                <br/>         
            </div>
         )
    }
}
 
export default View;