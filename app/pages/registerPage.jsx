import React, { Component } from 'react';
import RegisterForm from 'AppModules/userModule/registerForm.jsx';
import logo from 'AppSrc/img/logo.png';
import { Link } from 'react-router-dom';
class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (
            <div id="register">                
                <div className="container-fluid">
                    <br/>
                    <div className="w-100 text-center">
                    <Link to="/">
                        <img src={logo} className="logo" alt="" width="50%" style={{maxWidth:'200px'}}/>
                    </Link>
                    </div>
                    <div className="col-12 col-md-6" style={{margin:'0 auto'}}>
                    <RegisterForm/>
                    <div className="text-center">                        
                        <hr />
                        <span className="text-secondary">                                
                            2018 © QTS Team
                        </span>
                        <br />
                        <br />
                    </div>                    
                    </div>
                    
                </div>
            </div>
        );
    }
}
 
export default Page;