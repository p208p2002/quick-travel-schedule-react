import React, { Component } from 'react';
import Nav from 'AppModules/mainModules/topnav/main.jsx'
import AutoScheduleModule from 'AppModules/autoScheduleModule/index.jsx'

class AutoSchedule extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() {
        return ( 
            <div>
                <Nav/>
                <div className="container">
                    <AutoScheduleModule/>
                </div>
            </div> 
        );
    }
}
 
export default AutoSchedule;