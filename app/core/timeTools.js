// import moment from 'moment'

export function timeJudge (startTime_HHmm,endTime_HHmm,is24=true){
    if(!is24){
        return 'no support'
    }
    let sHH = startTime_HHmm.substring(0,2)
    // let smm = startTime_HHmm.substring(3,5)
    // console.log(sHH,smm)
    
    let eHH = endTime_HHmm.substring(0,2)
    // let emm = endTime_HHmm.substring(3,5)
    // console.log(eHH,emm)

    sHH = parseInt(sHH)
    eHH = parseInt(eHH)

    if((sHH>=8 && eHH <=12) && (sHH<eHH)){
        return '早上'
    }
    else if((sHH>=12 && eHH <=18) && (sHH<eHH)){
        return '下午'
    }
    else if((sHH>=18 && eHH <=22) && (sHH<eHH)){
        return '晚上'
    }
    else{
        return 'invaild'
    }
}

export function singleTimeJudge (startTime_HHmm,is24=true){
    if(!is24){
        return 'no support'
    }
    let sHH = startTime_HHmm.substring(0,2)
    sHH = parseInt(sHH)

    if(sHH>=8 && sHH <=12){
        return '早上'
    }
    else if(sHH>=12 && sHH <=18){
        return '下午'
    }
    else if(sHH>=18 && sHH <=22){
        return '晚上'
    }
    else{
        return 'invaild'
    }
}

export function timeInterval(startTime_HHmm,endTime_HHmm){
    let sHH = startTime_HHmm.substring(0,2)
    let smm = startTime_HHmm.substring(3,5)
    // console.log(sHH,smm)
    
    let eHH = endTime_HHmm.substring(0,2)
    let emm = endTime_HHmm.substring(3,5)
    // console.log(eHH,emm)

    let sMin = parseInt(sHH)*60 + parseInt(smm)
    let eMin = parseInt(eHH)*60 + parseInt(emm)
    // console.log('res',eMin-sMin)

    return (eMin-sMin)
}

export function getNowTimeStartAt(nowTimeFragment){
    switch (nowTimeFragment) {
        case '早上':
            return '08:00'            
        
        case '下午':
            return '12:00'                        

        case '晚上':
            return '18:00'
                
        default:
            console.error('not a time fragment')
            break;
    }
}

export function singleTimeJudgeId (startTime_HHmm,is24=true){
    if(!is24){
        return 'no support'
    }
    let sHH = startTime_HHmm.substring(0,2)
    sHH = parseInt(sHH)

    if(sHH>=8 && sHH <=12){
        return 1
    }
    else if(sHH>=12 && sHH <=18){
        return 2
    }
    else if(sHH>=18 && sHH <=22){
        return 3
    }
    else{
        return 'invaild'
    }
}

export function getTimeFragmentLimitAt(timeFragmentId){
    timeFragmentId = parseInt(timeFragmentId)
    switch (timeFragmentId) {
        case 1:
            return '12:00'
        
        case 2:
            return '18:00'
        
        case 3:
            return '22:00'
    
        default:
            return 'invaild'
    }
}

export function getTimeFragmentStartAt(timeFragmentId){
    timeFragmentId = parseInt(timeFragmentId)
    switch (timeFragmentId) {
        case 1:
            return '08:00'
        
        case 2:
            return '12:00'
        
        case 3:
            return '18:00'
    
        default:
            return 'invaild'
    }
}

export function timeFragmentIdToTag(timeFragmentId){
    timeFragmentId = parseInt(timeFragmentId)
    switch (timeFragmentId) {
        case 1:
            return '早上'
        
        case 2:
            return '下午'
        
        case 3:
            return '晚上'
    
        default:
            return 'invaild'
    }
}