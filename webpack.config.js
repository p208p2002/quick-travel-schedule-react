var path = require('path');
var webpack = require('webpack');

var config = {
    entry: [path.resolve(__dirname, 'app/main.jsx')],
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js'
    },
    plugins: [
          new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Tether: 'tether',
            Popper: ['popper.js', 'default'],
            // In case you imported plugins individually, you must also require them here:
            Util: "exports-loader?Util!bootstrap/js/dist/util",
            Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
          })
      ],
    module: {
        rules: [
            {
              test: /\.jsx?$/,
              // With this line, make sure you only include your javascript
              // source files
              include: [ path.resolve(__dirname, './app') ],
              use: {
                loader: 'babel-loader',
                options: {
                  presets: ['env'],
                  plugins: ['transform-runtime']
                }
              }
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            },
            // LESS
            {
            test: /\.less$/,
            use: [{
                loader: 'style-loader' // creates style nodes from JS strings
            }, {
                loader: 'css-loader' // translates CSS into CommonJS
            }, {
                loader: 'less-loader' // compiles Less to CSS
            }]
            },
            //SCSS
            {
                test: /\.scss$/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader" // compiles Sass to CSS
                ]
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: "eslint-loader",
                options: {
                  // eslint options (if necessary)
                }
            },
            { 
                test: /\.(png|jpg|gif)$/, 
                loader: "file-loader",
                // options: {
                //     name: 'dirname/[hash].[ext]'
                // }
            }
          ]
    },
    devServer: {
        historyApiFallback: true,
        stats: true,
        inline: true,
        progress: true
    },
    resolve: {
        extensions: ['.js', '.jsx', '.css', '.scss'],
        alias: 
        {
            App: path.resolve(__dirname,'app'),
            AppSrc: path.resolve(__dirname, 'app/src'),
            AppModules: path.resolve(__dirname, 'app/modules'),
            AppPages: path.resolve(__dirname, 'app/pages'),
            AppCore: path.resolve(__dirname, 'app/core'),
        }
    }
};

module.exports = config;